﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Threading;

namespace ScheduledExecutor.Utils
{
    public static class FileSystem
    {
        public static string UserFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Temp");
        public static string WorkingFolder = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        public static string ScenarioTempFolder;
        public static string FolderForFailedTasks;
        public static string TempFolderForUploads;

        public static void CleanOldTempFolders()
        {
            var userDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Temp");
            var leftowers = Directory.GetDirectories(userDir, "seleniumExecutorTemp*");
            try
            {
                foreach (var temp in leftowers)
                {
                    Trace.TraceInformation($"Deleting old temp: {temp}");
                    Directory.Delete(temp, true);
                }
            }
            catch (IOException ex)
            {
                Common.ClearSandbox();
                Thread.Sleep(2000);
                leftowers = Directory.GetDirectories(userDir, "seleniumExecutorTemp*");
                foreach (var temp in leftowers)
                {
                    Trace.TraceInformation($"Deleting old temp: {temp}");
                    Directory.Delete(temp, true);
                }
            }

            var uploadleftowers = Directory.GetDirectories(userDir, "seleniumFilesForUpload*");
            try
            {
                foreach (var temp in uploadleftowers)
                {
                    Trace.TraceInformation($"Deleting old upload dir: {temp}");
                    Directory.Delete(temp, true);
                }
            }
            catch (IOException ex)
            {
                Common.ClearSandbox();
                Thread.Sleep(2000);
                uploadleftowers = Directory.GetDirectories(userDir, "seleniumFilesForUpload*");
                foreach (var temp in uploadleftowers)
                {
                    Trace.TraceInformation($"Deleting old upload dir: {temp}");
                    Directory.Delete(temp, true);
                }
            }
        }

        public static void CreateTempFolder()
        {
            try
            {
                var tempDir = Path.Combine(UserFolder, $"seleniumExecutorTemp{DateTime.Now:yyyyMMddHHmmssffff}");

                ScenarioTempFolder = Directory.CreateDirectory(tempDir).FullName;

                Trace.TraceInformation($"Temp dir created: {ScenarioTempFolder}");                
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException($"Can't create temp dir for execution artifacts. Reason: {ex}");
            }
        }

        public static void CreateFolderForFailedTasks()
        {
            try
            {
                var failedCallbacksDir = Path.Combine(UserFolder, $"seleniumFailedCallbacks");

                FolderForFailedTasks = Directory.CreateDirectory(failedCallbacksDir).FullName;

                Trace.TraceInformation($"Uploads storage added: {FolderForFailedTasks}");                
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException($"Can't create failed callbacks dir. Reason: {ex}");
            }
        }

        public static void CreateTempFolderForUploads()
        {
            try
            {
                var tempFilesForUpload = Path.Combine(UserFolder, $"seleniumFilesForUpload{DateTime.Now:yyyyMMddHHmmssffff}");

                TempFolderForUploads = Directory.CreateDirectory(tempFilesForUpload).FullName;

                Trace.TraceInformation($"Failed callbacks dir created: {TempFolderForUploads}");
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException($"Can't create upload storage. Reason: {ex}");
            }
        }

        public static void CreateStatusFile(string status)
        {
            File.WriteAllText(Path.Combine(ScenarioTempFolder, "execution_status.txt"), status);
        }

        public static void ArchiveArtifacts()
        {
            try
            {
                Trace.TraceInformation("Archiving artifacts");
                using (var memoryStream = new MemoryStream())
                {
                    var zip = Path.Combine(ScenarioTempFolder, "artifacts.zip");
                    Trace.TraceInformation($"zip location {zip}");
                    using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                    {
                        foreach (var file in Directory.GetFiles(FileSystem.ScenarioTempFolder))
                        {
                            Trace.TraceInformation($"archiving {Path.GetFileName(file)}");
                            var archivedFile = archive.CreateEntry(Path.GetFileName(file));
                            using (var entryStream = archivedFile.Open())
                            {
                                using (var streamWriter = new BinaryWriter(entryStream))
                                {
                                    byte[] buffer;
                                    using (var fs = File.Open(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                                    {
                                        buffer = new byte[fs.Length];
                                        fs.Read(buffer, 0, (int)fs.Length);
                                    }

                                    streamWriter.Write(buffer);
                                }
                            }
                        }
                    }

                    using (var fileStream = new FileStream(zip, FileMode.Create))
                    {
                        memoryStream.Seek(0, SeekOrigin.Begin);
                        memoryStream.CopyTo(fileStream);
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError($"failed: {ex}");
            }
        }
    }
}
