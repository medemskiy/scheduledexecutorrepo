﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ScheduledExecutor.Utils
{
    public static class Common
    {
        public static void ClearSandbox()
        {
            Process.Start("cmd.exe", "/C taskkill /im firefox* /t /f");
            Process.Start("cmd.exe", "/C taskkill /im werfault* /t /f");
            //Process.Start("cmd.exe", "/C taskkill /im geckodriver* /t /f");

            foreach (TraceListener listener in Trace.Listeners)
            {
                listener.Close();
                listener.Dispose();
            }
            Trace.Listeners.Clear();
        }

        public static void SaveScenario(IDEScenario scenario)
        {
            var fileName = ClearInvalidCharactersFromPath(scenario.Name);
            var scenarioLocation = Path.Combine(FileSystem.ScenarioTempFolder, $"IDEScenario-{fileName}.html");
            try
            {
                Trace.TraceInformation($"Saving scenario to {scenarioLocation}");
                File.WriteAllText(scenarioLocation, scenario.RawScenario);
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Scenario saving error: {ex}");
            }
        }        

        public static string ClearInvalidCharactersFromPath(string path)
        {
            var invalid = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());

            return invalid.Aggregate(path, (current, c) => current.Replace(c.ToString(), ""));
        }

        public static string BonnCaptchaSolver(string captchaText)
        {
            var captcha = new List<string>();
            var position = 0;
            for (var i = 0; i < 7; i++)
            {
                captcha.Add(captchaText.Substring(position, 54));
                position += 54;
            }

            var dotMapString = new Dictionary<string, string> {
                {"A", "   *     * *    * *   *   *  ***** *     **     *" },
                {"B", "****** *     **     ******* *     **     ******* " },
                {"C", " ***** *     **      *      *      *     * ***** " },
                {"D", "****** *     **     **     **     **     ******* " },
                {"E", "********      *      ****   *      *      *******" },
                {"F", "********      *      ****   *      *      *      " },
                {"G", " ***** *     **      *      *   ****     * ***** " },
                {"H", "*     **     **     *********     **     **     *" },
                {"I", "*******   *      *      *      *      *   *******" },
                {"J", "      *      *      *      *      **     * ***** " },
                {"K", "*     **   ** * **   **     * **   *   ** *     *" },
                {"L", "*      *      *      *      *      *      *******" },
                {"M", "*     ***   *** * * **  *  **     **     **     *" },
                {"N", "*     ***    ** *   **  *  **   * **    ***     *" },
                {"O", " ***** *     **     **     **     **     * ***** " },
                {"P", "****** *     **     ******* *      *      *      " },
                {"Q", " ***** *     **     **     **   * **    *  **** *" },
                {"R", "****** *     **     ******* *   *  *    * *     *" },
                {"S", " ***** *     **       *****       **     * ***** " },
                {"T", "*******   *      *      *      *      *      *   " },
                {"U", "*     **     **     **     **     **     * ***** " },
                {"V", "*     **     * *   *  *   *   * *    * *     *   " },
                {"W", "*     **     **     **  *  ** * * ***   ***     *" },
                {"X", "*     * *   *   * *     *     * *   *   * *     *" },
                {"Y", "*     * *   *   * *     *      *      *      *   " },
                {"Z", "*******     *     *     *     *     *     *******" },
                {"0", "  ***   *   * *   * **  *  ** *   * *   *   ***  " },
                {"1", "   *     **    * *      *      *      *   *******" },
                {"2", " ***** *     *      *     *    **   **    *******" },
                {"3", " ***** *     *      *    **       **     * ***** " },
                {"4", "    *     **    * *   *  *  *******    *      *  " },
                {"5", "********      ******       *      **     * ***** " },
                {"6", "  ****  *     *      ****** *     **     * ***** " },
                {"7", "*******     *     *     *     *     *     *      " },
                {"8", " ***** *     **     * ***** *     **     * ***** " },
                {"9", " ***** *     **     * ******      *     *  ****  " }};

            var decodeList = new List<List<string>>();
            var charList = new List<string>();
            foreach (var line in captcha)
            {
                var tempList = new List<string>();
                position = 0;
                for (var i = 0; i < 6; i++)
                {
                    tempList.Add(line.Substring(position, 7));
                    position += 9;
                }

                decodeList.Add(tempList);
            }

            charList = decodeList
                .SelectMany(inner => inner.Select((item, index) => new { item, index }))
                .GroupBy(i => i.index, i => i.item)
                .Select(g => string.Join("", g.ToList()))
                .ToList();

            var response = string.Empty;
            foreach (var charCode in charList)
            {
                response += dotMapString.Where(k => k.Value.Equals(charCode)).FirstOrDefault().Key;
            }

            return response;
        }
    }
}
