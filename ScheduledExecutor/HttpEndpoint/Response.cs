﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace ScheduledExecutor
{
    public class Response
    {
        public HttpStatusCode StatusCode { get; set; }

        public string Message { get; set; }

        public string ContentType { get; set; }

        private Stream stream;
        public Stream Stream
        {
            get { return stream ?? new MemoryStream(Encoding.UTF8.GetBytes(Message ?? "")); }
            set { stream = value; }
        }

        public Dictionary<string, string> Headers { get; set; } = new Dictionary<string, string>();

        public override string ToString()
        {
            return $"{StatusCode}:{Message}";
        }
    }
}
