﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ScheduledExecutor
{
    public enum Status
    {
        Unknown = 0,
        Queued = 1,
        Executing = 2,
        Passed = 3,
        Failed = 4
    }

    public class ExecutionStatus
    {
        [JsonProperty("status")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Status Status { get; set; }

        [JsonProperty("note")]
        public string Note { get; set; }

        [JsonProperty("artifatcs")]
        public object Artifatcs { get; set; }

        [JsonProperty("driver_version")]
        public string DriverVersion { get; set; }
    }

    public class ApiRequest
    {
        [JsonProperty("location")]
        public Uri ScenarioLocation { get; set; }

        [JsonProperty("cb_url")]
        public Uri CallBackUri { get; set; }

        [JsonProperty("timeout")]
        public int CommandTimeout { get; set; } = 360000;

        [JsonProperty("delay")]
        public int DelayBetweenSteps { get; set; } = 1000;

        [JsonProperty("upload_targets")]
        public Dictionary<int, string> UploadTargets { get; set; }

        public int Id { get; set; }

        public ExecutionStatus ExecutionStatus { get; set; }
    }
}
