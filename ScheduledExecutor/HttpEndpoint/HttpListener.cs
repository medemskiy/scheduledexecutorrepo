﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using HealthMonitor;
using Newtonsoft.Json;
using System.Configuration;

namespace ScheduledExecutor
{
    //makecert.exe -n "CN=seleniumExecCA" -r -sv seleniumExecCA.pvk seleniumExecCA.cer -cy authority 
    //makecert.exe -sk seleniumExecSignedByCA -iv seleniumExecCA.pvk -n "CN=seleniumExecSignedByCA" -ic seleniumExecCA.cer seleniumExecSignedByCA.cer -sr localmachine -ss My
    //netsh http add sslcert ipport=0.0.0.0:28472 certhash=‎‎fe818fd8aa630f03582f57ebefd8696e38abd4dd appid={843ed092-83c0-4a26-b5fe-ade411f65b8a}
    //Basic c2VsZW5pdW1SdW5uZXI6MXFhejJ3c3ghQA==
    public class HttpListenerWorker
    {
        #region string class for command names

        private static class Commands
        {
            public const string RunScenario = "RunScenario";
            public const string GetStatus = "GetStatus";
            public const string GetHealth = "GetHealth";
            public const string Update = "Update";
            public const string Kill = "Kill";
        }

        #endregion

        #region Configuration properties

        private NetworkCredential allowedUser = new NetworkCredential(ConfigurationManager.AppSettings["userName"], ConfigurationManager.AppSettings["userPassword"]);
        private int PortNumber { get; set; } = 28472;

        #endregion Configuration properties

        #region private properties

        private HttpListener listener;
        private bool IsListening;

        #endregion private properties

        #region Commands queues

        public Queue<ApiRequest> ApiRequestsQueue { get; set; } = new Queue<ApiRequest>();

        public List<ApiRequest> ProcessedApiRequests { get; set; } = new List<ApiRequest>();

        #endregion Commands queues

        public int CmdCounter { get; set; } = 0;

        public string DriverVersion { get; set; }

        public void Initialize()
        {
            try
            {
                // Prepare the Listener
                listener = new HttpListener();
                listener.AuthenticationSchemes = AuthenticationSchemes.Basic;

                listener.Prefixes.Clear();
                //listener.Prefixes.Add($"https://35.157.158.184:{PortNumber}/SeleniumExecutor/");
                listener.Prefixes.Add($"https://localhost:{PortNumber}/SeleniumExecutor/");
                var hostnamePrefix = "";

                try
                {
                    // Find all active Ethernet network interfaces and add the IPv4 address to list of prefixes
                    foreach (var ni in NetworkInterface.GetAllNetworkInterfaces())
                    {
                        if (ni.OperationalStatus == OperationalStatus.Up)
                        {
                            foreach (var ip in ni.GetIPProperties().UnicastAddresses)
                            {
                                try
                                {
                                    if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                                    {
                                        Trace.TraceWarning($"Adding '{ip.Address}' to HTTP listener.");
                                        listener.Prefixes.Add($"https://{ip.Address}:{PortNumber}/SeleniumExecutor/");
                                    }
                                }
                                catch(Exception ex)
                                {
                                    Trace.TraceError($"{ex}");
                                }
                            }
                        }
                    }
                    // Finally, add the host name to list of prefixes.
                    // This might fail when starting Player in some network configurations but it is taken into account later on.
                    var hostName = GetHostName();
                    Trace.TraceWarning($"Adding '{hostName}' to HTTP listener.");
                    hostnamePrefix = $"https://{hostName}:{PortNumber}/SeleniumExecutor/";
                    listener.Prefixes.Add(hostnamePrefix);
                }
                catch (SocketException ex)
                {
                    Trace.TraceError(ex.GetBaseException().ToString());
                    Trace.TraceWarning("Failed to add some global prefixes to HTTP listener.");
                }

                try
                {
                    StartListening();
                }
                catch (SocketException ex)
                {
                    // If listener fails to start, remove host name from the list and try once more.
                    Trace.TraceError(ex.GetBaseException().ToString());
                    Trace.TraceWarning("Failed to start HTTP listener. Removing DNS host name prefix and trying again.");
                    listener.Prefixes.Remove(hostnamePrefix);
                    StartListening();
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
        }

        private void StartListening()
        {
            if (listener.IsListening)
                return;

            listener.Start();
            IsListening = true;
            Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    Thread.Sleep(100);
                    await HttpListener(listener);
                }
            }, TaskCreationOptions.LongRunning);

            Trace.TraceWarning(
                $"http adapter listening at{Environment.NewLine}- {string.Join(Environment.NewLine + "- ", listener.Prefixes)}");
            Trace.TraceWarning($"http adapter successfully started");
        }

        public Task<ApiRequest> GetCommand()
        {
            try
            {
                if (ApiRequestsQueue.Count <= 0)
                {
                    return Task.FromResult<ApiRequest>(null);
                }

                var commandReq = ApiRequestsQueue.Dequeue();
                commandReq.ExecutionStatus.DriverVersion = DriverVersion;
                commandReq.ExecutionStatus.Status = Status.Executing;
                ProcessedApiRequests.Add(commandReq);

                return Task.FromResult(commandReq);
            }
            catch (Exception ex)
            {
                Trace.TraceWarning($"error while getting command: {ex}");
                return null;
            }
        }

        #region Http listener

        private async Task HttpListener(HttpListener httpListener)
        {
            try
            {
                var context = await httpListener.GetContextAsync();

                ThreadPool.QueueUserWorkItem(wi => HandleRequest(context));
            }
            catch (HttpListenerException ex)
            {
                if (IsListening)
                {
                    Trace.TraceError($"HttpListener exception: {ex.Message}");
                }
            }
        }

        #endregion Http listener

        #region Request handling

        private void HandleRequest(HttpListenerContext context)
        {
            try
            {
                var identity = (HttpListenerBasicIdentity) context.User.Identity;
                if (!(allowedUser.UserName.Equals(identity.Name) && allowedUser.Password.Equals(identity.Password)))
                {
                    WriteResponse(context,
                        new Response {Message = "Unauthorized", StatusCode = HttpStatusCode.Unauthorized});
                    return;
                }

                var rawrequest = ExtractCommandRequest(context);
                // this only enqueue the request

                Console.WriteLine($"incoming api request: \"{rawrequest.Name}\"");
                Trace.TraceWarning($"incoming api request: \"{rawrequest.Name}\"");
                Trace.TraceInformation($"{rawrequest.HttpMethod}:{rawrequest.Name}:{rawrequest.RawRequestBody}");
                var responseObject = ProcessCommandRequest(rawrequest);

                WriteResponse(context, responseObject);

                if (responseObject.StatusCode != HttpStatusCode.OK)
                {
                    Trace.TraceError($"({rawrequest.Name}) {responseObject.StatusCode}: {responseObject.Message}");
                }

                if (responseObject.StatusCode == HttpStatusCode.Accepted)
                {
                    Trace.TraceWarning("Update command received, going to exit!");
                    Environment.Exit(777);
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
        }

        private RawRequest ExtractCommandRequest(HttpListenerContext context)
        {
            var request = context.Request;

            var urlParts = request.RawUrl.Replace("/SeleniumExecutor/", "").Split('?');
            var pathParts = urlParts[0].Split('/');

            var commandName = pathParts[0];

            object requestBody;

            switch (request.ContentType)
            {
                case ContentType.ApplicationOctetStream:
                    using (var reader = new BinaryReader(request.InputStream))
                    {
                        var buffer = new byte[2048];
                        int bytesRead;
                        var ms = new MemoryStream();
                        while ((bytesRead = reader.Read(buffer, 0, 2048)) > 0)
                        {
                            ms.Write(buffer, 0, bytesRead);
                        }
                        requestBody = ms;
                    }
                    break;
                default:
                    using (var reader = new StreamReader(request.InputStream, request.ContentEncoding))
                    {
                        requestBody = reader.ReadToEnd();
                    }
                    break;
            }

            var cmdReq = new RawRequest
            {
                Name = commandName,
                HttpMethod = request.HttpMethod,
                ContentType = request.ContentType,
                PathParts = pathParts.Skip(1).ToList(),
                RawQueryString = request.QueryString,
                RawRequestBody = requestBody
            };

            return cmdReq;
        }

        private Response ProcessCommandRequest(RawRequest rawRequest)
        {
            try
            {
                switch (rawRequest.Name)
                {
                    case Commands.RunScenario:
                    {
                        var cmds = HandleRunScenario(rawRequest);
                        if (cmds.Any())
                        {
                            foreach (var cmd in cmds)
                            {
                                ApiRequestsQueue.Enqueue(cmd);
                            }
                        }

                        var ids = string.Join(",", cmds.Select(c => c.Id).ToList());

                        var jobj = new JArray();
                        foreach (var cmd in cmds)
                        {
                            jobj.Add(new JObject(
                                new JProperty("id", cmd.Id),
                                new JProperty("status", cmd.ExecutionStatus.Status.ToString()),
                                new JProperty("scenario_url", cmd.ScenarioLocation),
                                new JProperty("cb_url", cmd.CallBackUri)));
                        }

                        return new Response
                        {
                            StatusCode = HttpStatusCode.OK,
                            Message = JsonConvert.SerializeObject(jobj),
                            ContentType = ContentType.ApplicationJson
                        };
                    }
                    case Commands.GetHealth:
                    {
                        var failedRequests = new List<ApiRequest>();
                        var topReasons = new List<string>();
                        try
                        {
                                failedRequests = ProcessedApiRequests.Where(req => req.ExecutionStatus.Status == Status.Failed).ToList();
                                topReasons =
                                    failedRequests.Select(
                                            req => req.ExecutionStatus.Note.Split(new[] { "|", "|" }, StringSplitOptions.None)[0])
                                        .GroupBy(note => note).ToList()
                                        .OrderByDescending(note => note.Count())
                                        .Select(note => $"({note.Count()}) {note.Key}")
                                        .Take(5).ToList();
                            }
                        catch (Exception)
                        {
                            // ignored
                        }

                        return new Response
                        {
                            StatusCode = HttpStatusCode.OK,
                            Message = JsonConvert.SerializeObject(
                                new JObject(
                                    new JProperty("timestamp", $"{DateTime.Now:s}"),
                                    new JProperty("pending_count", ApiRequestsQueue.Count),
                                    new JProperty("completed_count", ProcessedApiRequests.Count),
                                    new JProperty("failed_count", failedRequests.Count),
                                    new JProperty("top_failures", new JArray(topReasons)),
                                    new JProperty("driver_version", DriverVersion))),
                            ContentType = ContentType.ApplicationJson
                        };
                    }
                    case Commands.GetStatus:
                    {
                        return HandleGetCommandExecutionStatus(rawRequest);
                    }
                    case Commands.Update:
                    {
                        var result = new BitbucketConnector().DownloadLatestMaster();
                        return new Response
                        {
                            StatusCode =
                                result.StartsWith("latest revision already downloaded")
                                    ? result.Contains(BitbucketConnector.GetRunningRevision())
                                        ? HttpStatusCode.OK
                                        : HttpStatusCode.Accepted
                                    : HttpStatusCode.Accepted,
                            Message = new JObject(new JProperty("message", result)).ToString(),
                            ContentType = ContentType.ApplicationJson
                        };
                    }
                    case Commands.Kill:
                    {
                        new Task(() =>
                        {
                            Thread.Sleep(5000);
                            Process.Start("cmd.exe", "/C taskkill /im firefox* /t /f");
                            //Process.Start("cmd.exe", "/C taskkill /im geckodriver* /t /f");
                            Thread.Sleep(5000);
                            Environment.Exit((int) ExitCodes.PlannedKill);
                        }).Start();
                        return new Response
                        {
                            StatusCode = HttpStatusCode.OK,
                            Message = "Killing in 5 seconds..."
                        };
                    }
                    default:
                    {
                        return new Response
                        {
                            StatusCode = HttpStatusCode.BadRequest,
                            Message = "Wrong api endpoint name",
                            ContentType = ContentType.ApplicationJson
                        };
                    }
                }

            }
            catch (Exception ex)
            {
                return new Response
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Message = ex.Message
                };
            }
        }

        private List<ApiRequest> HandleRunScenario(RawRequest commandRequest)
        {
            var jobject = JObject.Parse(commandRequest.RawRequestBody as string);
            var scenarios = jobject.SelectToken("scenarios");

            if (scenarios == null && !scenarios.Children().Any())
            {
                throw new CommunicationException(
                    "List of commands sent as a parameter contains no commands (either null or zero commands). If the request was composed as JSON, check case (JSON is case-sensitive)");
            }

            var cmds = scenarios.Children().Select(c =>
            {
                ApiRequest cmd = new ApiRequest();
                try
                {
                    cmd = c.ToObject<ApiRequest>();
                }
                catch (JsonSerializationException)
                {
                    var targets = (c["upload_targets"] as JArray).ToList();
                    var dictionary = new Dictionary<int, string>();
                    var i = 1;
                    targets.ForEach(item => dictionary.Add(i++, item.ToString()));

                    c["upload_targets"] = JObject.FromObject(dictionary);
                    cmd = c.ToObject<ApiRequest>();
                }
                
                cmd.Id = CmdCounter;

                var cmdReq = new ApiRequest
                {
                    ScenarioLocation = cmd.ScenarioLocation,
                    CallBackUri = cmd.CallBackUri,
                    ExecutionStatus = new ExecutionStatus { Status = Status.Queued, DriverVersion = DriverVersion },
                    Id = cmd.Id,
                    CommandTimeout = cmd.CommandTimeout,
                    DelayBetweenSteps = cmd.DelayBetweenSteps,
                    UploadTargets = cmd.UploadTargets
                };

                CmdCounter++;
                return cmdReq;
            });

            return cmds.ToList();
        }

        private void WriteResponse(HttpListenerContext context, Response responseObj)
        {
            var response = context.Response;

            response.ContentLength64 = responseObj.Stream.Length;
            response.StatusCode = (int)responseObj.StatusCode;
            response.SendChunked = true;

            foreach (var header in responseObj.Headers)
            {
                response.AddHeader(header.Key, header.Value);
            }
            response.ContentType = responseObj.ContentType;

            Trace.TraceInformation(responseObj.ToString());

            using (var output = response.OutputStream)
            {
                responseObj.Stream.CopyTo(output);
                output.Close();
            }

        }

        public ExecutionStatus GetCommandExecutionStatus(int commandId)
        {
            var status = new ExecutionStatus { DriverVersion = DriverVersion };

            if (ApiRequestsQueue.Count == 0 && ProcessedApiRequests.Count == 0)
            {
                throw new InvalidOperationException("There are no commands in the list to query for status");
            }

            // Is it a valid index?
            if (commandId >= CmdCounter || commandId < 0)
            {
                throw new IndexOutOfRangeException(
                    $"Command execution status requested for a Command whose index ({commandId}) is out of range - index is zero-based and the highest assigned command id is {CmdCounter - 1}.");
            }

            if (ProcessedApiRequests.Any(cmd => cmd.Id == commandId))
            {
                status = ProcessedApiRequests.FirstOrDefault(cmd => cmd.Id == commandId)?.ExecutionStatus ?? new ExecutionStatus { Status = Status.Unknown, DriverVersion = DriverVersion };
            }
            else
            {
                status.Status = Status.Queued;
            }

            Trace.TraceInformation("method execution finished");

            return status;
        }

        private Response HandleGetCommandExecutionStatus(RawRequest commandRequest)
        {
            var cmdIndexArg = commandRequest.RawQueryString["id"];
            int cmdIndex;

            var status = new ExecutionStatus { DriverVersion = DriverVersion };

            if (cmdIndexArg == null)
            {
                status.Status = Status.Failed;
                status.Note = "You must specify the command argument \"id\", as a valid, positive integer";
                return GetCommandExecutionStatusResponse(HttpStatusCode.BadRequest, status);
            }

            if (!int.TryParse(cmdIndexArg, NumberStyles.Integer, null, out cmdIndex))
            {
                status.Status = Status.Failed;
                status.Note = $"Failed to parse the argument \"id\", you MUST specify the value as a valid, positive integer (value received: \"{cmdIndexArg}\")";
                return GetCommandExecutionStatusResponse(HttpStatusCode.BadRequest, status);
            }

            try
            {
                return GetCommandExecutionStatusResponse(HttpStatusCode.OK, GetCommandExecutionStatus(cmdIndex));
            }
            catch (Exception ex)
            {
                ex = ex.InnerException ?? ex;
                status.Status = Status.Failed;
                status.Note = ex.Message;
                return GetCommandExecutionStatusResponse(HttpStatusCode.BadRequest, status);
            }
        }

        private Response GetCommandExecutionStatusResponse(HttpStatusCode statusCode, ExecutionStatus status, string message)
        {
            return GetCommandExecutionStatusResponse(statusCode, status);
        }

        private Response GetCommandExecutionStatusResponse(HttpStatusCode statusCode, ExecutionStatus status)
        {
            var cmdExecStatus = new Response
            {
                Message = JsonConvert.SerializeObject(status),
                StatusCode = statusCode,
                ContentType = ContentType.ApplicationJson
            };

            Trace.TraceInformation($"{cmdExecStatus.StatusCode}:{cmdExecStatus.Message}");

            return cmdExecStatus;
        }

        #endregion Request handling

        #region Adapter configuration
        private static string GetHostName()
        {
            var hostName = Dns.GetHostName();

            var domainName = IPGlobalProperties.GetIPGlobalProperties().DomainName;

            if (!string.IsNullOrWhiteSpace(domainName) && !hostName.EndsWith(domainName))
            {
                hostName += "." + domainName;
            }

            return hostName.ToLower();
        }
        #endregion Adapter configuration
    }
}
