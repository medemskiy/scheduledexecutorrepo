﻿namespace ScheduledExecutor
{
    public static class ContentType
    {
        public const string ApplicationJson = "application/json";

        public const string ApplicationOctetStream = "application/octet-stream";
    }
}
