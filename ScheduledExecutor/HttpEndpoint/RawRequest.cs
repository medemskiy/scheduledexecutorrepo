﻿using System.Collections.Generic;
using System.Collections.Specialized;

namespace ScheduledExecutor
{
    public class RawRequest
    {
        public string Name { get; set; }

        public string HttpMethod { get; set; }

        public string ContentType { get; set; }

        public IList<string> PathParts { get; set; }
        
        public object RawRequestBody { get; set; }

        public NameValueCollection RawQueryString { get; set; }
    }
}
