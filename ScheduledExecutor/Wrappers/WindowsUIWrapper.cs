﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using ScheduledExecutor.UIAutomation;
using UIAutomationClient;

namespace ScheduledExecutor.Wrappers
{
    public class WindowsUIWrapper : IDisposable
    {
        public bool Handling { get; set; }
        public Thread workerThread { get; set; }
        public int DelayBetweenPolls { get; set; }
        public Queue<string> UploadTargets { get; set; }
        public string SaveLocation { get; set; }

        private List<string> uploadedFiles = new List<string>();

        public void Dispose()
        {
            Trace.TraceInformation("Dispose called...");
            while (Handling)
            {
                Thread.Sleep(1000);
            }

            workerThread?.Abort();
        }
                
        public async Task HandleFileDialogs()
        {
            new Task(() =>
            {
                Trace.TraceInformation("UIA task started...");
                workerThread = Thread.CurrentThread;
                while (true)
                {
                    try
                    {
                        if (Handling) continue;

                        if (UploadTargets.Count == 0)
                        {
                            Trace.TraceInformation("No files to upload...");
                            
                            Thread.Sleep(DelayBetweenPolls);
                            continue;
                        }

                        var uploadDialog = WaitWinActive("File Upload", 2000);
                        if (uploadDialog != null)
                        {
                            Trace.TraceInformation("Upload dialog discovered...");
                            Handling = true;
                            var currentTarget = UploadTargets.Dequeue();

                            Trace.TraceInformation($"Specifying file path: {currentTarget}");
                            Thread.Sleep(2000);
                            SubmitForm(uploadDialog, currentTarget);
                            Thread.Sleep(2000);

                            uploadDialog = WaitWinActive("File Upload", 2000);
                            if (uploadDialog != null)
                            {
                                Trace.TraceInformation("Window wasn't closed, retrying...");
                                SubmitForm(uploadDialog, currentTarget);
                            }

                            WaitWinNotActive("File Upload", 20000);

                            Thread.Sleep(10000);
                            Trace.TraceInformation("Upload dialog closed...");

                            Trace.TraceInformation("Marking file(s) for deletion...");
                            foreach (var file in currentTarget.Split(' '))
                            {
                                uploadedFiles.Add(file);
                                Trace.TraceInformation($"marked: {file}");
                            }

                            Handling = false;
                        }
                        else
                        {
                            Trace.TraceInformation("not found...");
                        }
                    }
                    catch (ThreadAbortException ex)
                    {
                        Trace.TraceInformation($"Exiting listener... {ex}");
                        Handling = false;
                    }
                    catch (Exception ex)
                    {
                        Trace.TraceInformation($"Autoit exception: {ex}");
                        Handling = false;
                    }
                }
            }).Start();
        }

        private AutomationElement WaitWinActive(string name, int timeout)
        {
            var sw = Stopwatch.StartNew();
            while (sw.ElapsedMilliseconds <= timeout)
            {
                try
                {
                    var root = new AutomationElement(new CUIAutomation8().GetRootElement());
                    var target = root?.FindElementByName(TreeScope.TreeScope_Descendants, name);

                    if (target?.Name != null)
                        return target;
                }
                catch (Exception ex)
                {
                    Trace.TraceInformation(ex.ToString());
                }
            }
            return null;
        }

        private bool WaitWinNotActive(string name, int timeout)
        {
            var sw = Stopwatch.StartNew();
            while (sw.ElapsedMilliseconds <= timeout)
            {
                try
                {
                    var root = new AutomationElement(new CUIAutomation8().GetRootElement());
                    var target = root?.FindElementByName(TreeScope.TreeScope_Descendants, name);

                    if (target?.Name == null)
                        return true;
                }
                catch (Exception ex)
                {
                    Trace.TraceInformation(ex.ToString());
                }
            }
            return false;
        }

        private void SubmitForm(AutomationElement parent, string filePath)
        {
            try
            {
                var edit = parent.FindElementByCondition(TreeScope.TreeScope_Descendants, new AutomationCondition("Name", "File name:").And("ClassName", "Edit"));
                var button = parent.FindElementByCondition(TreeScope.TreeScope_Descendants, new AutomationCondition("Name", "Open").And("ClassName", "Button"));

                edit.SetValue(filePath);
                button.InvokeClick();
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
        }       
    }
}
