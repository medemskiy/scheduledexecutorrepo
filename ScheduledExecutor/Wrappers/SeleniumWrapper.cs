﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;
using ScheduledExecutor.Utils;

namespace ScheduledExecutor.Wrappers
{
    public class SeleniumWrapper : IDisposable
    {
        #region Private variables

        private Thread executionThread;
        private IWebDriver webDriver;
        private int timeout;
        private int delay;
        private Dictionary<string, string> storedValues = new Dictionary<string, string>();
        private Dictionary<string, string> driverWindows = new Dictionary<string, string>();
        private string gotoInstruction;

        private bool executionFinished;

        #endregion Private variables

        public SeleniumWrapper(Type driver, ApiRequest request)
        {
            timeout = request.CommandTimeout;
            delay = request.DelayBetweenSteps;
            Common.ClearSandbox();

            FileSystem.CleanOldTempFolders();

            FileSystem.CreateTempFolder();
            FileSystem.CreateFolderForFailedTasks();
            FileSystem.CreateTempFolderForUploads();

            TextWriterTraceListenerExt.Init(Path.Combine(FileSystem.ScenarioTempFolder, "execution_log.txt"), SourceLevels.All);

            GitHub.GitHubClient.DownloadGeckoDriver();

            Trace.TraceInformation($"timeout: {timeout} dealy: {delay} scenario temp: {FileSystem.ScenarioTempFolder}");

            if (typeof(FirefoxDriver) == driver)
            {
                var options = new FirefoxOptions
                {
                    Profile = new FirefoxProfile
                    {
                        AcceptUntrustedCertificates = true
                    }
                };

                options.Profile.SetPreference("browser.download.dir", FileSystem.ScenarioTempFolder);
                options.Profile.SetPreference("browser.download.folderList", 2);
                options.Profile.SetPreference("browser.helperApps.alwaysAsk.force", false);
                options.Profile.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");
                options.Profile.SetPreference("intl.accept_languages", "de-DE");
                options.Profile.SetPreference("pdfjs.disabled", true);
                options.BrowserExecutableLocation = @"c:\Program Files (x86)\Mozilla Firefox\firefox.exe";

                webDriver = new FirefoxDriver(options);

                webDriver.Manage().Window.Maximize();
            }
            else
            {
                webDriver = (IWebDriver)Activator.CreateInstance(driver);
            }
        }

        #region Scenario execution

        public void ExecuteIdeScenario(IDEScenario scenario)
        {
            Trace.TraceInformation($"Executing: {scenario.Name}");
            var cts = new CancellationTokenSource();

            var task = Task.Run(() =>
            {
                executionThread = Thread.CurrentThread;
                var uploadQueue = new Queue<string>();
                scenario.UplaodTargets?.OrderBy(item => item.Key)
                    .ToList()
                    .ForEach(item => uploadQueue.Enqueue(item.Value));

                using (var autoit = new WindowsUIWrapper
                {
                    SaveLocation = FileSystem.ScenarioTempFolder,
                    UploadTargets = uploadQueue,
                    DelayBetweenPolls = 500
                })
                {
                    autoit.HandleFileDialogs();
                    var skipSteps = 0;

                    var stepNumber = 1;

                    while (!executionFinished)
                    {
                        var exitForeach = false;

                        foreach (var command in scenario.Commands.Skip(skipSteps))
                        {
                            var cmd = SubstituteStoredValues(command);

                            if (exitForeach)
                            {
                                break;
                            }
                            try
                            {
                                var autoitHandled = false;
                                while (autoit.Handling)
                                {
                                    Trace.TraceInformation("waiting for autoit to complete...");
                                    autoitHandled = true;
                                    Thread.Sleep(1000);
                                }

                                if (autoitHandled)
                                {
                                    TakePostStepScreenShot($"{stepNumber++:00} - fileUpload{DateTime.Now:HHmmss}");
                                    autoitHandled = false;
                                }

                                switch (cmd.Command.ToLower())
                                {
                                    case "gotoif":
                                        try
                                        {
                                            if (webDriver.ExecuteJavaScript<bool>($"return {cmd.Target}"))
                                            {
                                                skipSteps = GetSkipStepsCount(cmd.Value.ToLower(), scenario);
                                                exitForeach = true;
                                            }
                                        }
                                        catch
                                        {
                                            gotoInstruction = string.Empty;
                                        }
                                        break;

                                    case "gotolabel":
                                        skipSteps = GetSkipStepsCount(cmd.Value.ToLower(), scenario);
                                        exitForeach = true;
                                        break;

                                    case "label":
                                        break;

                                    default:
                                        try
                                        {
                                            ExecuteCommand(cmd, scenario.BaseUrl, autoit);
                                            Thread.Sleep(2000);
                                        }
                                        catch (StaleElementReferenceException)
                                        {
                                            Trace.TraceWarning("Got stale element exception, retrying...");
                                            Thread.Sleep(20000);
                                            //webDriver.Navigate().Refresh();
                                            ExecuteCommand(cmd, scenario.BaseUrl, autoit);
                                        }
                                        finally
                                        {
                                            Trace.TraceInformation("Restoring default context...");
                                            webDriver.SwitchTo().DefaultContent();
                                        }
                                        break;
                                }
                            }
                            catch (Exception ex)
                            {
                                Trace.TraceError($"Command execution failed, reason {ex}");
                                executionFinished = true;
                                throw new ScenarioExecutionFailedException($"Command {cmd.Command} execution failed. Reason: {ex.Message}", ex);
                            }
                            finally
                            {
                                TakePostStepScreenShot($"{stepNumber++:00} - {cmd.Command} | {cmd.Target} | {cmd.Value}");
                            }
                        }

                        if (!exitForeach)
                        {
                            executionFinished = true;
                            Thread.Sleep(delay);
                        }
                    }
                }
            });

            var executionCompleted = false;

            Parallel.Invoke(
                () =>
                {
                    task.Wait(cts.Token);
                    executionCompleted = true;
                },
                () =>
                {
                    var sw = Stopwatch.StartNew();
                    while (sw.ElapsedMilliseconds <= timeout + 5000)
                    //giving 5 seconds more in case if it takes too long to wait element, so execution will fail with proper message
                    {
                        if (task.Exception != null)
                        {
                            throw task.Exception;
                        }
                        if (executionCompleted)
                        {
                            Trace.TraceInformation("ExecuteIdeScenario task completed");
                            return;
                        }
                        Thread.Sleep(500);
                    }

                    Trace.TraceInformation($"ExecuteIdeScenario task timed out after: {sw.ElapsedMilliseconds}");
                    executionThread.Abort();
                });
        }

        private int GetSkipStepsCount(string label, IDEScenario scenario)
        {
            var step = scenario.Commands.Where(cmd => cmd.Command.ToLower().Equals("label")).FirstOrDefault(cmd => cmd.Value.ToLower().Equals(label));
            return scenario.Commands.IndexOf(step);
        }

        private bool HandleNavigationInstruction(IDECommand command)
        {
            if (string.IsNullOrEmpty(gotoInstruction))
            {
                return true;
            }

            if (command.Command.ToLower().Equals("label") && command.Target.ToLower().Equals(gotoInstruction))
            {
                gotoInstruction = string.Empty;
                return true;
            }

            return false;
        }

        public void ExecuteCommand(IDECommand command, string baseUrl = "", WindowsUIWrapper autoit = null)
        {
            var msg = $"ExecuteCommand - command: {command.Command}, element: {command.Target}, value: {command.Value}";
            Console.WriteLine(msg);
            Trace.TraceInformation(msg);

            switch (command.Command.ToLower())
            {
                case "open":
                    {
                        webDriver.Url = command.Target.StartsWith("http")
                            ? HttpUtility.HtmlDecode(command.Target)
                            : $"{(string.IsNullOrEmpty(baseUrl) ? string.Empty : baseUrl.TrimEnd('/') + "/")}{HttpUtility.HtmlDecode(command.Target)?.TrimStart('/')}";
                        driverWindows.Add(webDriver.Url, webDriver.CurrentWindowHandle);
                        break;
                    }

                case "openwindow":
                    {
                        webDriver.ExecuteJavaScript($"window.open(\"{command.Target}\")");
                        var handles = webDriver.WindowHandles;
                        var newHandle = handles.FirstOrDefault(h => !driverWindows.Values.Contains(h));
                        driverWindows.Add(command.Target, newHandle);
                        webDriver.SwitchTo().Window(newHandle);

                        break;
                    }

                case "mouseover":
                    {
                        var el = WaitElement(command.Target, 0, 2);

                        new Actions(webDriver)
                                    .MoveToElement(el, 0, 0)
                                    .MoveByOffset(5, 5);

                        break;
                    }

                case "mouseout":
                    {
                        var el = WaitElement(command.Target, 0, 2);

                        new Actions(webDriver)
                                    .MoveToElement(el, 0, 0)
                                    .MoveByOffset(-15, -15);

                        break;
                    }

                //case "mouseover":
                //case "mouseout":
                //    break;

                case "click":
                case "clickat":
                case "clickandwait":
                    {
                        var el = WaitElement(command.Target, 0, 2);

                        try
                        {
                            try
                            {
                                JsScrollToView(el);
                                el.SendKeys("");
                                Trace.TraceInformation("workaround for invisible element");
                            }
                            catch (ElementNotVisibleException)
                            {
                                Trace.TraceInformation("Handled element not visible exception");
                                throw;
                            }
                            catch (Exception ex)
                            {
                                Trace.TraceInformation($"Workaround for hidden button exception, not critical. {ex}");
                            }

                            if (string.IsNullOrEmpty(command.Value))
                            {
                                var skipEnter = false;

                                try
                                {
                                    el.Click();
                                    Trace.TraceInformation("element clicked");
                                    skipEnter = true;
                                    Thread.Sleep(500);
                                }
                                catch (InvalidElementStateException)
                                {
                                    Trace.TraceWarning("Caught invalid state exception, sending Enter...");

                                    el.SendKeys(Keys.Enter);
                                    skipEnter = true;
                                }
                                catch (Exception ex)
                                {
                                    Trace.TraceWarning($"Caught exception, sending Enter... {ex}");
                                }

                                if (autoit.Handling)
                                    return;

                                if (!skipEnter)
                                {
                                    try
                                    {
                                        Trace.TraceInformation("trying send Enter ...");
                                        el.SendKeys(Keys.Enter);
                                    }
                                    catch (Exception ex)
                                    {
                                        Trace.TraceInformation($"... not performed, {ex}");
                                    }
                                }
                            }
                            else
                            {
                                new Actions(webDriver)
                                    .MoveToElement(el, 0, 0)
                                    .MoveByOffset(5, 5)
                                    .Click()
                                    .Build()
                                    .Perform();
                                Trace.TraceInformation("action with moveto performed");

                                if (command.Target.ToLower().Equals("id=isc_e") || command.Target.ToLower().Contains("div[@id='isc_h']"))
                                {
                                    Trace.TraceInformation("Workaround for saarland (login)");
                                    try
                                    {
                                        new Actions(webDriver)
                                            .MoveToElement(el, 0, 0)
                                            .MoveByOffset(5, 5)
                                            .Click()
                                            .Build()
                                            .Perform();
                                    }
                                    catch
                                    { }
                                }
                                if (command.Target.Equals("class=gwt-FileUpload"))
                                {
                                    Trace.TraceInformation("Workaround for saarland (download)");
                                    Thread.Sleep(5000);
                                    try
                                    {
                                        new Actions(webDriver)
                                            .MoveToElement(el, 0, 0)
                                            .MoveByOffset(5, 5)
                                            .Click()
                                            .Build()
                                            .Perform();
                                    }
                                    catch (Exception ex)
                                    {
                                        Trace.TraceInformation(ex.ToString());
                                    }
                                }
                            }
                        }
                        catch (ElementNotVisibleException)
                        {
                            JsScrollToView(el);
                            JsClick(el);
                            Trace.TraceInformation("js click performed");
                        }
                        break;
                    }

                case "type":
                case "typeandwait":
                case "sendkeys":
                    {
                        var typeEl = WaitElement(command.Target, 0, 2);
                        SetText(typeEl, command.Value.Trim());
                        break;
                    }

                case "select":
                case "selectandwait":
                    {
                        var element = WaitElement(command.Target, 0, 2);
                        var select = new SelectElement(element);


                        var locatorType = string.Empty;
                        var locator = string.Empty;

                        if (command.Value.Split('=').Length < 2)
                        {
                            locatorType = "label";
                            locator = command.Value;
                        }
                        else
                        {
                            locatorType = command.Value.Split('=')[0].ToLower();
                            locator = command.Value.Split('=')[1];
                        }

                        locator = locator.Replace("*", "");

                        switch (locatorType)
                        {
                            case "label":
                                try
                                {
                                    select.SelectByText(locator);
                                }
                                catch (NoSuchElementException)
                                {
                                    var option = select.Options.FirstOrDefault(op => op.Text.Contains(locator));
                                    if (option == null)
                                        throw new NoSuchElementException($"available options: {string.Join(";", select.Options.Select(op => op.Text))}");

                                    option.Click();
                                }
                                break;
                            case "value":
                                select.SelectByValue(locator);
                                break;
                            case "index":
                                select.SelectByIndex(int.Parse(locator));
                                break;
                            default:
                                throw new NotSupportedException(
                                    $"Select element locator \"{locatorType}\" is not supported...");
                        }
                        break;
                    }

                case "selectwindow":
                    {
                        if (string.IsNullOrEmpty(command.Value) || command.Value.Split('=').Length < 2)
                        {
                            Trace.TraceWarning("wrong value, skipping");
                            return;
                        }
                        webDriver.SwitchTo().Window(command.Value.Split('=')[1]);
                        break;
                    }

                case "pause":
                    {
                        Thread.Sleep(int.Parse(command.Target));
                        break;
                    }

                case "waitforelementpresent":
                    {
                        WaitElement(command.Target, 0, 2);
                        break;
                    }

                case "captureentirepagescreenshot":
                    {
                        TakeScreenShot(Path.GetFileNameWithoutExtension(command.Target));
                        break;
                    }

                case "store":
                    {
                        var directive = command.Target.Split('=')[0];

                        switch (directive)
                        {
                            case "clickifexists":
                                {
                                    try
                                    {
                                        var testEl = WaitElement(command.Target.Replace(directive + "=", ""), 30000);
                                        testEl.Click();
                                    }
                                    catch { }
                                    break;
                                }

                            case "screenshot":
                                {
                                    TakeScreenShot(command.Target.Replace(directive + "=", ""));
                                    break;
                                }

                            case "pagesource":
                                {
                                    SavePageSource(command.Target.Replace(directive + "=", ""));
                                    break;
                                }

                            case "fileforupload":
                                {
                                    var file = RequestWrapper.DownloadFile(command.Target.Replace(directive + "=", ""), FileSystem.TempFolderForUploads);
                                    autoit.UploadTargets.Clear();
                                    autoit.UploadTargets.Enqueue(file);
                                    break;
                                }

                            case "handlealert":
                                {
                                    var sw = Stopwatch.StartNew();
                                    var action = command.Target.Replace(directive + "=", "");
                                    while (sw.ElapsedMilliseconds < 2000)
                                    {
                                        try
                                        {
                                            var alert = webDriver.SwitchTo().Alert();

                                            if (alert == null) continue;
                                            switch (action)
                                            {
                                                case "accept":
                                                    alert.Accept();
                                                    return;
                                                default:
                                                    alert.Dismiss();
                                                    return;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            // ignored
                                        }
                                    }
                                    break;
                                }

                            case "solveBonnCaptcha":
                                {
                                    var bonnCaptchaFieldClassName = "realperson-text";
                                    var bonnCaptchaReplyFieldId = "defaultReal";

                                    var captchaField = WaitElement($"class={bonnCaptchaFieldClassName}", 0);
                                    var captchaText = captchaField.Text.Replace("\r\n", "");

                                    var captchaReply = Common.BonnCaptchaSolver(captchaText);

                                    var replyField = WaitElement($"id={bonnCaptchaReplyFieldId}", 0);
                                    replyField.SendKeys(captchaReply);

                                    break;
                                }

                            case "solveCaptcha":
                                {
                                    var parameters = command.Target.Replace(directive + "=", "")
                                        .Split(';')
                                        .ToDictionary(key => key.Split(':')[0].Trim(), value => value.Split(':')[1].Trim());

                                    var captchaImageFieldLocator = parameters["imageField"];
                                    var captchaRefreshFieldLocator = parameters["refreshField"];
                                    var captchaResponseFieldLocator = parameters["answerField"];

                                    var filepath = SaveCaptchaImage(captchaImageFieldLocator);

                                    var captchaAnswer = TwoCaptchaWrapper.SolveCaptcha(filepath);

                                    if (captchaAnswer.Equals("ERROR_CAPTCHA_UNSOLVABLE"))
                                    {
                                        WaitElement(captchaRefreshFieldLocator, 0).Click();

                                        filepath = SaveCaptchaImage(captchaImageFieldLocator);

                                        captchaAnswer = TwoCaptchaWrapper.SolveCaptcha(filepath);
                                    }

                                    WaitElement(captchaResponseFieldLocator, 0).SendKeys(captchaAnswer);

                                    break;
                                }

                            default:
                                {
                                    UpdateStoredVariable(command.Value, command.Target);
                                    break;
                                }
                        }
                        break;
                    }

                case "storeeval":
                    {
                        command.Target = JSFunctionCallWrap(command.Target);
                        command.Value = JSFunctionCallWrap(command.Value);

                        try
                        {
                            var jsout = webDriver.ExecuteJavaScript<object>(command.Target);
                            UpdateStoredVariable(command.Value, jsout.ToString());
                        }
                        catch (Exception ex)
                        {
                            Trace.TraceError(ex.ToString());
                        }
                        break;
                    }

                case "storevalue":
                    {
                        var targetEl = WaitElement(command.Target, 0, 2);
                        UpdateStoredVariable(command.Value, targetEl.GetAttribute("value"));
                        break;
                    }

                case "runscript":
                    {
                        webDriver.ExecuteJavaScript<string>(command.Target);
                        break;
                    }

                case "check":
                    {
                        SetCheckboxValue(WaitElement(command.Target, 0, 2), true);
                        break;
                    }

                case "uncheck":
                    {
                        SetCheckboxValue(WaitElement(command.Target, 0, 2), false);
                        break;
                    }

                case "setTimeout":
                    {
                        timeout = int.Parse(command.Target);
                        break;
                    }

                case "break":
                    {
                        Console.WriteLine("Press [Enter] to continue...");
                        Console.ReadLine();
                        break;
                    }

                case "echo":
                    {
                        Console.WriteLine(command.Target);
                        break;
                    }

                case "assertalert":
                    {
                        Console.WriteLine("Alert check temporarry unavailable...");
                        break;
                    }

                case "asserttext":
                    {
                        var elementTextContainer = WaitElement(command.Target, 0, 2);
                        var text = JsGetText(elementTextContainer);

                        Trace.TraceInformation($"assertText: expected {command.Value}, got: {text}");
                        if (text.ToLower().Equals(command.Value.ToLower()))
                        {
                            Trace.TraceInformation("assertText: ok");
                            return;
                        }

                        throw new ScenarioExecutionFailedException($"assertText failed: expected {command.Value}, got: {text}");
                    }

                default:
                    {
                        var message = $"Command \"{command.Command}\" is not supported...";
                        Trace.TraceError(message);
                        throw new NotSupportedException(message);
                    }
            }

            Trace.TraceInformation($"{command.Command.ToLower()} - done");
        }

        private string SaveCaptchaImage(string captchaImageFieldLocator)
        {
            var captchaField = WaitElement(captchaImageFieldLocator, 0);
            var base64string = ((IJavaScriptExecutor)webDriver).ExecuteScript(
                "var c = document.createElement('canvas');" +
                "var ctx = c.getContext('2d');" +
                $"var img = document.getElementById('{captchaField.GetAttribute("id")}');" +
                "c.height=img.height; " +
                "c.width=img.width; " +
                "ctx.drawImage(img, 0, 0,img.width, img.height); " +
                "var base64String = c.toDataURL(); " +
                "return base64String;");

            var base64 = base64string.ToString().Split(',').Last();

            var filepath = $"captcha{(long)(DateTime.Now - new DateTime(1970, 1, 1)).TotalMilliseconds}.png";

            using (var stream = new MemoryStream(Convert.FromBase64String(base64)))
            {
                using (var bitmap = new Bitmap(stream))
                {

                    bitmap.Save(filepath, ImageFormat.Png);
                }
            }

            return filepath;
        }

        static ManualResetEvent resetEvent = new ManualResetEvent(true);

        public static void ReadKeyReset()
        {
            resetEvent.Set();
        }

        #endregion Scenario execution

        #region Commands

        private void SetCheckboxValue(IWebElement element, bool state)
        {
            if (element.Selected != state)
            {
                element.Click();
            }
        }

        private void SetText(IWebElement element, string text)
        {
            try
            {
                element.Clear();
                Trace.TraceInformation("element cleared");
            }
            catch (Exception ex)
            {
                Trace.TraceWarning($"Can't clear element, reason: {ex}");
            }

            try
            {
                element.Click();
                Trace.TraceInformation("element clicked");
            }
            catch (Exception ex)
            {
                Trace.TraceWarning($"Can't click element, reason: {ex}");
            }

            try
            {
                //sometimes value get lost after execution, adding Tab key sending to avoid value lost (workaround for Munster)
                element.SendKeys(text + Keys.Tab);
                Trace.TraceInformation($"keys {text} sent");
            }
            catch (Exception ex)
            {
                Trace.TraceWarning($"Can't send keys, reason: {ex}");
                throw;
            }
        }

        public void SavePageSource(string fileName)
        {
            var currentWindowHandle = webDriver.CurrentWindowHandle;
            var handles = webDriver.WindowHandles;
            Trace.TraceInformation($"Active handles found: {handles.Count}");

            fileName = Common.ClearInvalidCharactersFromPath(fileName);

            foreach (var handle in handles)
            {
                Trace.TraceInformation($"Switching to: {handle}");
                webDriver.SwitchTo().Window(handle);

                var pageSourceLocation = Path.Combine(FileSystem.ScenarioTempFolder, $"PageSource-{fileName}_{handle}.html");
                var pageSourceTopLocation = Path.Combine(FileSystem.ScenarioTempFolder, $"PageSource-{fileName}_{handle}_parent.html");
                try
                {
                    Trace.TraceInformation($"Saving page source to {pageSourceLocation}");
                    File.WriteAllText(pageSourceLocation, webDriver.PageSource);

                    Trace.TraceInformation($"Saving parent page source to {pageSourceTopLocation}");
                    webDriver.SwitchTo().DefaultContent();
                    File.WriteAllText(pageSourceTopLocation, webDriver.PageSource);
                }
                catch (InvalidOperationException ex)
                {
                    if (ex.Message.Equals("TypeError: can't access dead object"))
                    {
                        Trace.TraceWarning("Got dead object notification, retrying with page refresh...");
                        webDriver.Navigate().Refresh();
                        File.WriteAllText(pageSourceLocation, webDriver.PageSource);
                    }
                }
                catch (Exception ex)
                {
                    Trace.TraceError($"Page source saving error: {ex}");
                }

                try
                {
                    var innerHtml = Path.Combine(FileSystem.ScenarioTempFolder, $"PageSource-{fileName}_{handle}_innerHtml.html");
                    File.WriteAllText(innerHtml, webDriver.FindElement(By.TagName("body")).GetAttribute("innerHTML"));
                }
                catch { }

                try
                {
                    var iframes = webDriver.FindElements(By.TagName("iframe"));
                    var i = 1;
                    foreach (var frame in iframes)
                    {
                        try
                        {
                            webDriver.SwitchTo().ParentFrame();
                            webDriver.SwitchTo().Frame(frame);
                            var frameLocation = Path.Combine(FileSystem.ScenarioTempFolder, $"PageSource-{fileName}_{handle}_frame{i++}.html");
                            File.WriteAllText(frameLocation, webDriver.PageSource);
                        }
                        catch { }
                    }
                }
                catch { }

                try
                {
                    var frames = webDriver.FindElements(By.TagName("frame"));
                    var i = 1;
                    foreach (var frame in frames)
                    {
                        try
                        {
                            webDriver.SwitchTo().ParentFrame();
                            webDriver.SwitchTo().Frame(frame);
                            var frameLocation = Path.Combine(FileSystem.ScenarioTempFolder, $"PageSource-{fileName}_{handle}_frame_native{i++}.html");
                            File.WriteAllText(frameLocation, webDriver.PageSource);
                        }
                        catch { }
                    }
                }
                catch { }
            }

            webDriver.SwitchTo().Window(currentWindowHandle);
        }

        public void TakeScreenShot(string fileName)
        {
            fileName = Common.ClearInvalidCharactersFromPath(fileName);
            var screenShotLocation = Path.Combine(FileSystem.ScenarioTempFolder, $"stepScreenShot-{fileName}.png");
            try
            {
                Trace.TraceInformation($"Saving screenshot to {screenShotLocation}");
                GetFullViewPortScreenshot().Save(screenShotLocation, ImageFormat.Png);

                //webDriver.TakeScreenshot().SaveAsFile(screenShotLocation, ScreenshotImageFormat.Png);
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Screenshot saving error: {ex}");
            }
        }

        public void TakePostStepScreenShot(string stepName)
        {
            try
            {
                //if (TakeDesktopScreenShot(stepName))
                //    return;

                TakeDesktopScreenShot(stepName);

                Trace.TraceInformation("Taking browser hosted content only...");
                stepName = Common.ClearInvalidCharactersFromPath($"stepScreenShot-browser-{stepName}");
                stepName = stepName.Substring(0, stepName.Length > 150 ? 150 : stepName.Length - 1).Trim();

                var screenShotLocation = Path.Combine(FileSystem.ScenarioTempFolder, $"{stepName}.png");
                webDriver.TakeScreenshot().SaveAsFile(screenShotLocation, ScreenshotImageFormat.Png);
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Screenshot saving error: {ex}");
            }
        }

        private bool TakeDesktopScreenShot(string stepName)
        {
            try
            {
                stepName = Common.ClearInvalidCharactersFromPath($"stepScreenShot-os-{stepName}");
                stepName = stepName.Substring(0, stepName.Length > 150 ? 150 : stepName.Length - 1).Trim();

                var screenShotLocation = Path.Combine(FileSystem.ScenarioTempFolder, $"{stepName}.png");

                using (var bitmap = new Bitmap(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width,
                                                System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height))
                {
                    using (var g = Graphics.FromImage(bitmap))
                    {
                        g.CopyFromScreen(0, 0, 0, 0, bitmap.Size, CopyPixelOperation.SourceCopy);

                        bitmap.Save(screenShotLocation, ImageFormat.Png);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Trace.TraceError($"OS screenshot saving error: {ex}");
                return false;
            }
        }

        private Image GetFullViewPortScreenshot()
        {
            // Get the total size of the page
            var totalWidth = (int)(long)((IJavaScriptExecutor)webDriver).ExecuteScript("return document.body.offsetWidth");
            //documentElement.scrollWidth");
            var totalHeight = (int)(long)((IJavaScriptExecutor)webDriver).ExecuteScript("return  document.body.parentNode.scrollHeight");
            // Get the size of the viewport
            var viewportWidth = (int)(long)((IJavaScriptExecutor)webDriver).ExecuteScript("return document.body.clientWidth");
            //documentElement.scrollWidth");
            var viewportHeight = (int)(long)((IJavaScriptExecutor)webDriver).ExecuteScript("return window.innerHeight");
            //documentElement.scrollWidth");

            webDriver.SwitchTo().DefaultContent();
            // We only care about taking multiple images together if it doesn't already fit
            if (totalWidth <= viewportWidth && totalHeight <= viewportHeight)
            {
                var screenshot = webDriver.TakeScreenshot();
                return ScreenshotToImage(screenshot);
            }
            // Split the screen in multiple Rectangles
            var rectangles = new List<Rectangle>();
            // Loop until the totalHeight is reached
            for (var y = 0; y < totalHeight; y += viewportHeight)
            {
                var newHeight = viewportHeight;
                // Fix if the height of the element is too big
                if (y + viewportHeight > totalHeight)
                {
                    newHeight = totalHeight - y;
                }
                // Loop until the totalWidth is reached
                for (var x = 0; x < totalWidth; x += viewportWidth)
                {
                    var newWidth = viewportWidth;
                    // Fix if the Width of the Element is too big
                    if (x + viewportWidth > totalWidth)
                    {
                        newWidth = totalWidth - x;
                    }
                    // Create and add the Rectangle
                    var currRect = new Rectangle(x, y, newWidth, newHeight);
                    rectangles.Add(currRect);
                }
            }
            // Build the Image
            var stitchedImage = new Bitmap(totalWidth, totalHeight);
            // Get all Screenshots and stitch them together
            var previous = Rectangle.Empty;
            foreach (var rectangle in rectangles)
            {
                // Calculate the scrolling (if needed)
                if (previous != Rectangle.Empty)
                {
                    var xDiff = rectangle.Right - previous.Right;
                    var yDiff = rectangle.Bottom - previous.Bottom;
                    // Scroll
                    ((IJavaScriptExecutor)webDriver).ExecuteScript($"window.scrollBy({xDiff}, {yDiff})");
                }

                // Take Screenshot
                Screenshot screenshot = null;

                try
                {
                    screenshot = webDriver.TakeScreenshot();
                }
                catch (InvalidOperationException ex)
                {
                    if (ex.Message.Equals("TypeError: can't access dead object"))
                    {
                        Trace.TraceWarning("Got dead object notification, retrying with page refresh...");
                        webDriver.Navigate().Refresh();
                        screenshot = webDriver.TakeScreenshot();
                    }
                }

                // Build an Image out of the Screenshot
                var screenshotImage = ScreenshotToImage(screenshot);
                // Calculate the source Rectangle
                var sourceRectangle = new Rectangle(viewportWidth - rectangle.Width, viewportHeight - rectangle.Height,
                    rectangle.Width, rectangle.Height);
                // Copy the Image
                using (var graphics = Graphics.FromImage(stitchedImage))
                {
                    graphics.DrawImage(screenshotImage, rectangle, sourceRectangle, GraphicsUnit.Pixel);
                }
                // Set the Previous Rectangle
                previous = rectangle;
            }
            return stitchedImage;
        }

        private static Image ScreenshotToImage(Screenshot screenshot)
        {
            Image screenshotImage;
            using (var memStream = new MemoryStream(screenshot.AsByteArray))
            {
                screenshotImage = Image.FromStream(memStream);
            }
            return screenshotImage;
        }
        #endregion Commands

        #region IDisposable

        public void Dispose()
        {
            webDriver.Quit();
        }

        #endregion IDisposable

        #region Values substitution

        private string ReplaceWithStoredVariable(Regex regex, string input)
        {
            Trace.TraceInformation($"original input: {input}");

            var matches = regex.Matches(input);

            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    var storedKeyName = match.Groups[1].Value;
                    if (storedValues.ContainsKey(storedKeyName))
                    {
                        input = input.Replace(match.Value, $"{storedValues[storedKeyName]}");
                    }
                }
            }

            Trace.TraceInformation($"new input: {input}");
            return input;
        }

        private string JSFunctionCallWrap(string input)
        {
            Trace.TraceInformation($"original input: {input}");
            var regex = new Regex(@"[\w]*\(.*?\);");
            var match = regex.Matches(input);

            if (match.Count > 0)
            {
                input = input.Replace(match[match.Count - 1].Value, $"return {match[match.Count - 1].Value}");
            }
            else
            {
                if (!input.Contains("return ") && (input.Split('.').Length > 1 || input.Split(' ').Length > 1))
                {
                    input = $"return {input}";
                }
            }

            if (input.Contains("\"\""))
            {
                input = input.Replace("\"\"", "\"");
            }

            Trace.TraceInformation($"new input: {input}");
            return input;
        }

        private void UpdateStoredVariable(string key, string value)
        {
            if (storedValues.ContainsKey(key))
            {
                storedValues[key] = value;

                Trace.TraceInformation($"edit: {storedValues[key]} = {value}");
            }
            else
            {
                storedValues.Add(key, value);
                Trace.TraceInformation($"new: {storedValues[key]} = {value}");
            }
        }

        private IDECommand SubstituteStoredValues(IDECommand command)
        {
            Trace.TraceInformation($"original Target: {command.Target}, original Value: {command.Value}");
            var variableRegex = new Regex(@"\$\{(.*?)\}");
            var scriptedVariablesRegex = new Regex(@"storedVars\['(.*?)'\]");

            command.Target = ReplaceWithStoredVariable(variableRegex, command.Target);
            command.Value = ReplaceWithStoredVariable(variableRegex, command.Value);

            command.Target = ReplaceWithStoredVariable(scriptedVariablesRegex, command.Target);
            command.Value = ReplaceWithStoredVariable(scriptedVariablesRegex, command.Value);

            Trace.TraceInformation($"new Target: {command.Target}, new Value: {command.Value}");

            return command;
        }

        #endregion Values substitution

        #region IWebElement 

        private By BuildSearchCondition(string element)
        {
            var locatorType = element.Split('=').Length > 1 ? element.Split('=')[0].ToLower() : "xpath";
            var locator = element.Replace($"{locatorType}=", "");

            Trace.TraceInformation($"BuildSearchCondition - locator type: {locatorType}, locator: {locator}");

            switch (locatorType)
            {
                case "id":
                    return By.Id(locator);
                case "name":
                    return By.Name(locator);
                case "css":
                    return By.CssSelector(locator);
                case "class":
                    return By.ClassName(locator);
                case "link":
                    return By.LinkText(locator);
                case "tag":
                    return By.TagName(locator);
                case "xpath":
                    return By.XPath(locator);
                default:
                    if (element.StartsWith("//"))
                    {
                        Trace.TraceWarning("Assuming that locator is correct but parsing failed because complex xpath conditions...");
                        return By.XPath(element);
                    }

                    var message = $"Locator \"{locatorType}\" is not supported...";
                    Trace.TraceError(message);
                    throw new NotSupportedException(message);
            }
        }

        private IWebElement WaitElementJS(string element)
        {
            var sw = Stopwatch.StartNew();
            while (sw.ElapsedMilliseconds <= timeout)
            {
                var el = webDriver.ExecuteJavaScript<IWebElement>($"return {element}");
                if (el != null)
                {
                    return el;
                }

                Thread.Sleep(1000);
            }

            var message = $"Element searched \"{element}\", but wasn't found within {timeout}ms...";
            Trace.TraceError(message);
            throw new NoSuchElementException(message);
        }

        private IWebElement WaitElement(string element, int to = 0)
        {
            if (to == 0)
            {
                to = timeout;
            }

            if (element.StartsWith("document"))
            {
                return WaitElementJS(element);
            }

            var condition = BuildSearchCondition(element);

            var sw = Stopwatch.StartNew();

            while (sw.ElapsedMilliseconds <= to)
            {
                try
                {
                    webDriver.SwitchTo().ParentFrame();

                    var el = webDriver.FindElement(condition);

                    return el;
                }
                catch (NoSuchElementException)
                {
                    foreach (var handle in webDriver.WindowHandles)
                    {
                        webDriver.SwitchTo().Window(handle);

                        var frames = webDriver.FindElements(By.TagName("frame"));

                        foreach (var frame in frames)
                        {
                            webDriver.SwitchTo().Frame(frame);
                            try
                            {
                                var el = webDriver.FindElement(condition);


                                return el;
                            }
                            catch (NoSuchElementException)
                            {
                                webDriver.SwitchTo().ParentFrame();
                            }
                        }

                        var iframes = webDriver.FindElements(By.TagName("iframe"));
                        foreach (var frame in iframes)
                        {
                            webDriver.SwitchTo().Frame(frame);
                            try
                            {
                                var el = webDriver.FindElement(condition);
                                return el;
                            }
                            catch (NoSuchElementException)
                            {
                                webDriver.SwitchTo().ParentFrame();
                            }
                        }
                    }

                    Trace.TraceInformation("Element not found, retrying in 10 seconds...");
                    Thread.Sleep(10000);
                }
            }
            var message = $"Element searched \"{condition}\", but wasn't found within {timeout}ms...";
            Trace.TraceError(message);
            throw new NoSuchElementException(message);
        }

        private IWebElement WaitElement(string element, int to = 0, int retries = 2)
        {
            var currentTry = 1;
            to = to == 0 ? 60000 : to;

            while (currentTry <= retries)
            {
                Trace.TraceInformation($"Trying find element: {element}, try: {currentTry}");
                try
                {
                    var targetElement = WaitElement(element, to);
                    if (targetElement != null)
                    {
                        Trace.TraceInformation("Element found. Ok...");
                        LogProperties(targetElement);

                        return targetElement;
                    }

                    Trace.TraceInformation("Element not found. Got null...");
                    webDriver.Navigate().Refresh();
                }
                catch (NoSuchElementException)
                {
                    Trace.TraceInformation("Element not found. Timeout...");
                    webDriver.Navigate().Refresh();
                }
                catch (Exception ex)
                {
                    Trace.TraceInformation($"Element not found. Exception... {ex}");
                }
                finally
                {
                    currentTry++;
                }
            }

            var message = $"Element searched \"{element}\", but wasn't found within {to}ms in {retries} retries...";
            Trace.TraceError(message);
            throw new NoSuchElementException(message);

        }

        private static void LogProperties(IWebElement targetElement)
        {
            var output = new List<string>();
            try
            {
                var size = targetElement.Size;
                output.Add($"height: {size.Height}");
                output.Add($"width: {size.Width}");
            }
            catch { }

            try
            {
                var location = targetElement.Location;

                output.Add($"x: {location.X}");
                output.Add($"y: {location.Y}");
            }
            catch { }

            try
            {
                output.Add($"displayed: {targetElement.Displayed}");
            }
            catch { }

            try
            {
                output.Add($"enabled: {targetElement.Enabled}");
            }
            catch { }

            try
            {
                output.Add($"selected: {targetElement.Selected}");
            }
            catch { }

            try
            {
                output.Add($"tag: {targetElement.TagName}");
            }
            catch { }

            Trace.TraceInformation(string.Join(", ", output));
        }

        private void JsScrollToView(IWebElement element)
        {
            try
            {
                var js = (IJavaScriptExecutor)webDriver;
                js.ExecuteScript("arguments[0].scrollIntoView(true)", element);
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }

        }

        private void JsClick(IWebElement element)
        {
            try
            {
                var js = (IJavaScriptExecutor)webDriver;
                js.ExecuteScript("arguments[0].click()", element);
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
        }

        private void JsSetText(IWebElement element, string value)
        {
            try
            {
                var js = (IJavaScriptExecutor)webDriver;
                js.ExecuteScript($"arguments[0].textContent={value}", element);
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
        }

        private string JsGetText(IWebElement element)
        {
            try
            {
                var js = (IJavaScriptExecutor)webDriver;
                var value = js.ExecuteScript($"return arguments[0].textContent", element);
                Trace.TraceInformation($"returning {value.ToString()}");
                return value.ToString();
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
                return string.Empty;
            }
        }

        #endregion IWebElement         
    }
}
