﻿using System.Collections.Generic;

namespace ScheduledExecutor
{
    public class IDECommand
    {
        public string Command { get; set; } = string.Empty;

        public string Target { get; set; } = string.Empty;

        public string Value { get; set; } = string.Empty;
    }

    public class IDEScenario
    {
        public string Name { get; set; }
        public List<IDECommand> Commands { get; set; }
        public string BaseUrl { get; set; }
        public string RawScenario { get; set; }
        public Dictionary<int, string> UplaodTargets { get; set; }
    }
}
