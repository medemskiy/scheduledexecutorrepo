﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ScheduledExecutor
{
    public static class TwoCaptchaWrapper
    {
        private static string apiKey = "10586a614300a026a6fc83599d9415cf";
        private static string postUrl = $"http://2captcha.com/in.php?key={apiKey}&json=1&language=2";
        private static string getUrl = $"http://2captcha.com/res.php?key={apiKey}&action=get&json=1&id={{0}}";

        public static async Task<string> PostCaptchaImage(string imagePath)
        {
            using (var client = new HttpClient())
            {
                var multiPartContent = new MultipartFormDataContent("---------------------------" + DateTime.Now.Ticks.ToString("x"));

                var fileStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
                var streamContent = new StreamContent(fileStream);
                multiPartContent.Add(streamContent, "file", "file.png");
                streamContent.Headers.ContentType = new MediaTypeHeaderValue("image/jpg");

                var response = await client.PostAsync(postUrl, multiPartContent);
                var message = JObject.Parse(await response.Content.ReadAsStringAsync());

                return message["request"]?.ToString();
            }
        }

        public static async Task<string> GetProcessingResult(string uploadId)
        {
            var url = string.Format(getUrl, uploadId);

            using (var client = new HttpClient())
            {
                var response = await client.GetStringAsync(url);

                var captchaResult = JObject.Parse(response)?["request"].ToString();

                while (captchaResult.Equals("CAPCHA_NOT_READY"))
                {
                    Thread.Sleep(5000);
                    response = await client.GetStringAsync(url);
                    captchaResult = JObject.Parse(response)?["request"].ToString();
                }

                return captchaResult;
            }
        }

        public static string SolveCaptcha(string imagePath)
        {
            var taskId = PostCaptchaImage(imagePath).Result;
            return GetProcessingResult(taskId).Result;
        }
    }
}