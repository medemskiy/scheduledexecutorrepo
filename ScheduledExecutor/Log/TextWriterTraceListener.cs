﻿using System;
using System.Diagnostics;

namespace ScheduledExecutor
{
    public class TextWriterTraceListenerExt : TextWriterTraceListener
    {
        #region properties
        public static string ResultsFolder = string.Empty;

        private static string CallingMethod
        {
            get
            {
                try
                {
                    var st = new StackTrace(true);
                    var frame = new StackFrame();

                    for (var i = 0; i < st.FrameCount; i++)
                    {
                        frame = st.GetFrame(i);
                        var name = frame.GetMethod().DeclaringType.Name;
                        if (name != typeof(TraceListener).Name &&
                            name != typeof(TextWriterTraceListenerExt).Name &&
                            name != typeof(Trace).Name &&
                            !name.Equals("TraceInternal"))
                        {
                            break;
                        }
                    }
                    // handle lamda
                    var method = frame.GetMethod();
                    var methodName = method.Name;
                    var type = method.DeclaringType.Name;
                    if (methodName.StartsWith("<"))
                    {
                        var end = methodName.IndexOf(">b");
                        if (end > 0)
                        {
                            methodName = methodName.Substring(1, end - 1);
                        }

                        // lamda type and method name usually come in pair
                        if (type.StartsWith("<>c"))
                        {
                            type = method.DeclaringType.DeclaringType?.Name;
                        }
                    }

                    return $"{type}:{methodName}";
                }
                catch (Exception)
                {
                    return "unknown";
                }
            }
        }

        private static string TimeStamp => DateTime.UtcNow.ToString("o");

        #endregion properties

        #region base
        public TextWriterTraceListenerExt(string filename, SourceLevels levels) : base(string.Format(filename, DateTime.UtcNow.ToString("yyyyMMdd_hhmmss")))
        {
            if (Trace.Listeners["TextWriterTraceListener"] != null)
            {
                DisposeCurrent();
            }

            Name = "TextWriterTraceListener";
            Filter = new EventTypeFilter(levels);
        }

        public static void Init(string filename, SourceLevels levels)
        {
            var listener = new TextWriterTraceListenerExt(filename, levels);
            Trace.Listeners.Add(listener);
        }

        public override void Write(string message)
        {

        }

        public override void WriteLine(string message)
        {
            base.WriteLine(FormatOutput(message, CallingMethod));
        }
        #endregion base

        #region extension
        public void DisposeCurrent()
        {
            try
            {
                Flush();
                Close();
                Trace.Listeners.Remove("TextWriterTraceListener");
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Unable to dispose of TextWriterTraceListener properly: \"{ex.Message}\"");
                Trace.TraceInformation(ex.ToString());
            }
        }

        private string FormatOutput(string message, string entrypoint)
        {
            return $"{TimeStamp}|{entrypoint}|{message.Trim()}";
        }
        #endregion extension
    }
}

