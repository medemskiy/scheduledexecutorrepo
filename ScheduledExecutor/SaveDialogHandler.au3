If WinWaitActive("Save As", "", $CmdLine[2]) Then
   Sleep(1000)
   Send($CmdLine[1])
   Sleep(1000)
   Send("{ENTER}")
   Sleep(5000)
   ConsoleWrite("Ok!")
   Exit(0)
EndIf
ConsoleWriteError("Save As dialog wasn't found!")
Exit(1)