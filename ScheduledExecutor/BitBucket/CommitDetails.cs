﻿using System;
using Newtonsoft.Json;

namespace ScheduledExecutor.BitBucket
{
    public class CommitDetails
    {
        [JsonProperty("hash")]
        public string Hash { get; set; }

        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        public override string ToString()
        {
            return $"{Type}    {Date:G}    {Hash}";
        }
    }
}
