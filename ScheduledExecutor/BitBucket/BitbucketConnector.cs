﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json.Linq;
using ScheduledExecutor.BitBucket;

namespace ScheduledExecutor
{
    public class BitbucketConnector
    {
        public string GetAuthToken()
        {
            var authToken = string.Empty;
            var authResponse = new JObject();
            var request = WebRequest.Create("https://bitbucket.org/site/oauth2/access_token");

            var parameter = "grant_type=client_credentials";
            var data = Encoding.ASCII.GetBytes(parameter);

            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;
            request.Method = HttpMethod.Post.ToString();

            
            var encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes("Gg3hUrNJNvDzGj4CLd" + ":" + "B2nTJsJPLMGNU4bE4ACLZsKsx7db8dkT"));
            request.Headers.Add("Authorization", "Basic " + encoded);

           
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, parameter.Length);
            }

            using (var response = request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        authResponse = JObject.Parse(reader.ReadToEnd());
                    }
                }
            }
            authToken = authResponse.GetValue("access_token").ToString(); 
            return authToken;
        }

        public string GetLatestCommit()
        {
            var trackingBranch = ConfigurationManager.AppSettings["branch"];
            var commitsUrl = $"https://api.bitbucket.org/2.0/repositories/medemskiy/scheduledexecutorrepo/commits/{trackingBranch}";
            var request = WebRequest.Create(commitsUrl);
            request.CachePolicy = new HttpRequestCachePolicy(HttpRequestCacheLevel.NoCacheNoStore);
            var commitsResponse = new JObject();
            request.Method = HttpMethod.Get.ToString();

            using (var response = request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        commitsResponse = JObject.Parse(reader.ReadToEnd());
                    }
                }
            }
            var commits = commitsResponse["values"].ToObject<List<CommitDetails>>().OrderByDescending(cmt => cmt.Date);
            Trace.TraceInformation(string.Join(Environment.NewLine, commits.Select(cmt => cmt.ToString())));

            var commitDateOrdered = commits.FirstOrDefault();

            return commitDateOrdered?.Hash.Substring(0, 12);
        }

        public string DownloadLatestMaster()
        {
            var latestCommit = GetLatestCommit();
            var downloadUrl = $"https://bitbucket.org/medemskiy/scheduledexecutorrepo/get/{latestCommit}.zip";

            if (!Directory.Exists("revisions"))
            {
                Directory.CreateDirectory("revisions");
            }

            if (!Directory.Exists("revisions\\history"))
            {
                Directory.CreateDirectory("revisions\\history");
            }

            if (!Directory.Exists("revisions\\latest"))
            {
                Directory.CreateDirectory("revisions\\latest");
            }

            if (!Directory.Exists("revisions\\laststable"))
            {
                Directory.CreateDirectory("revisions\\laststable");
            }

            if (!File.Exists($"revisions\\history\\{latestCommit}.zip"))
            {
                foreach (var file in Directory.GetFiles("revisions\\latest"))
                {
                    File.Delete(file);
                }

                Trace.TraceInformation($"downloading file to revisions\\history\\{latestCommit}.zip");
                new WebClient().DownloadFile(downloadUrl, $"revisions\\history\\{latestCommit}.zip");

                ExtractDistribution(latestCommit);
                return $"revisions\\history\\{latestCommit}.zip";
            }
            else
            {
                return $"latest revision already downloaded:{latestCommit}";
            }
        }

        public void ExtractDistribution(string commitHash)
        {
            var zipPath = $"revisions\\history\\{commitHash}.zip";
            var extractPath = @"revisions\latest";
            var directory = "Distribution";
            using (var archive = ZipFile.OpenRead(zipPath))
            {
                var result = from currEntry in archive.Entries
                             where Path.GetDirectoryName(currEntry.FullName).EndsWith(directory)
                             where !string.IsNullOrEmpty(currEntry.Name)
                             select currEntry;


                foreach (var entry in result)
                {
                    Trace.TraceInformation($"extracting {entry.Name}");
                    entry.ExtractToFile(Path.Combine(extractPath, entry.Name));
                }
            }

            File.WriteAllText(Path.Combine(extractPath, "revision"), commitHash);
        }

        public static string GetRunningRevision()
        {
            try
            {
                var ownLocation = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                var revision = File.ReadAllText(Path.Combine(ownLocation, "revision"));
                Trace.TraceInformation(revision);
                return revision;
            }
            catch(Exception ex)
            {
                Trace.TraceInformation($"{ex}");
                return "unknown, error";
            }
        }
    }
    
}
