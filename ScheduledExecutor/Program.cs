﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using OpenQA.Selenium.Firefox;
using ScheduledExecutor.Wrappers;

namespace ScheduledExecutor
{
    public static class Program
    {
        public static string OwnLocation = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        private static async void Experiments()
        {            
            //var list = new List<string> {"a||1", "b||1", "b||1", "b||1", "b||1", "a||1", "b", "c||1", "a||1", "b||1", "d||1", "b||1", "d" };

            //var topReasons =
            //                         list.Select(
            //                                 req => req.Split(new[] { "|", "|" }, StringSplitOptions.None)[0])
            //                             .GroupBy(note => note).ToList()
            //                             .OrderByDescending(note => note.Count())
            //                             .Distinct()
            //                             .Take(5);


            //var t = new BitbucketConnector().GetLatestCommit();
            var command = new IDECommand { Command = "storeEval", Target = "return document.title", Value = "var1" };
            try
            {
                var driver = new SeleniumWrapper(typeof(FirefoxDriver),
                new ApiRequest { CommandTimeout = 60000 });

                driver.ExecuteIdeScenario(new IDEScenario
                {
                    Commands = new List<IDECommand>
            {
                
                //new IDECommand {Command = "echo", Target = "----label0"},
                //new IDECommand {Command = "label", Target = "label1"},
                //new IDECommand {Command = "echo", Target = "----label1"},
                //new IDECommand {Command = "label", Target = "label2"},
                //new IDECommand {Command = "echo", Target = "----label2"}
                //new IDECommand {Command = "store", Target = "Deutsch", Value = "nationality"},
                //new IDECommand {Command = "storeEval", Target = "function getNationality() {if(storedVars[\'nationality\']==\"Deutsch\"){return \"Deutsch\"; } else {return \"EU-Staaten\"}}; getNationality();", Value = "evaluatedNationality"}
            },
                    Name = "qwe"
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine("--- exception ---");
            }
        }

        static void Main(string[] args)
        {
            //Experiments();

            AppDomain.CurrentDomain.UnhandledException += (sender, eventArgs) =>
            {
                Console.WriteLine(eventArgs.ToString());
                Thread.Sleep(20000);
                Trace.TraceError(eventArgs.ToString());
                Environment.Exit(1);
            };

            Trace.Listeners.Clear();
            TextWriterTraceListenerExt.Init($"TaskExecutor_{DateTime.Now:yyyyMMdd-HHmmss}.log", SourceLevels.All);
            Trace.Listeners.Add(new ConsoleTraceListener());
            Trace.AutoFlush = true;

#if DEBUG
            args = new[] { @"test" };
#endif

            var runParameters = ParseCmdArgs(args);
            Console.WriteLine($"Started with:{Environment.NewLine}" +
                              $"Interactive: {runParameters["interactive"]}{Environment.NewLine}" +
                              $"StartAt: {(DateTime)runParameters["startat"]:F}{Environment.NewLine}" +
                              $"Interval: {(TimeSpan)runParameters["interval"]}{Environment.NewLine}" +
                              $"ScenariosDirectory: {runParameters["scenarios"]}");

            if ((bool)runParameters["use_health_monitor"] && Process.GetProcessesByName("HealthMonitor").Length == 0)
            {
                Console.WriteLine("Please run HealthMonitor.exe");
                Console.ReadLine();
                Environment.Exit(1);
            }

            InteractiveExecutor.Start();

            while (true)
            {
                Thread.Sleep(500);
            }
        }

        private static Dictionary<string, object> ParseCmdArgs(string[] args)
        {
            var argsDictionary = args.ToList()
                .ToDictionary(arg => arg.Split('=')[0].ToLower(), arg => arg.Split('=').Length > 1 ? arg.Split('=')[1].ToLower() : null);

            var startDate = !argsDictionary.ContainsKey("startat") || argsDictionary["startat"].Equals("now")
                ? DateTime.Now
                : DateTime.ParseExact(argsDictionary["startat"], "dd/MM/yyyy-HH:mm", CultureInfo.InvariantCulture);

            var interval = !argsDictionary.ContainsKey("interval")
                ? TimeSpan.Zero
                : argsDictionary["interval"].Contains("d")
                    ? TimeSpan.FromDays(int.Parse(argsDictionary["interval"].Replace("d", "")))
                    : argsDictionary["interval"].Contains("h")
                        ? TimeSpan.FromHours(int.Parse(argsDictionary["interval"].Replace("h", "")))
                        : argsDictionary["interval"].Contains("m")
                            ? TimeSpan.FromMinutes(int.Parse(argsDictionary["interval"].Replace("m", "")))
                            : TimeSpan.Zero;

            var scenarios = argsDictionary.ContainsKey("scenarios") ? argsDictionary["scenarios"] : null;

            var interactive = argsDictionary.ContainsKey("interactive");
            var useHealthMonitor = !argsDictionary.ContainsKey("test");

            return new Dictionary<string, object>
            {
                {"startat", startDate},
                {"interval", interval},
                {"scenarios", scenarios},
                {"interactive", interactive},
                {"use_health_monitor", useHealthMonitor}
            };
        }
    }
}
