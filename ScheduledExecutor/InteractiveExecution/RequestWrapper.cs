﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace ScheduledExecutor
{
    public static class RequestWrapper
    {
        private static string Request(string url, string method, ICredentials credentials = null)
        {
            Trace.TraceInformation($"{method.ToUpper()}:{url}");

            var request = WebRequest.Create(url);

            if (credentials != null)
            {
                request.Credentials = credentials;
            }

            request.Method = method;
            request.ContentType = method.Equals("post") ? ContentType.ApplicationOctetStream : ContentType.ApplicationJson;

            var returnValue = string.Empty;
            
            using (var response = request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        returnValue = reader.ReadToEnd();
                    }
                }
            }
           
            return returnValue;
        }

        public static string Get(string apiPath, ICredentials credentials = null)
        {
            return Request(apiPath, "get", credentials);
        }

        public static string Post(string apiPath, string filePath, NameValueCollection parameters = null)
        {
            using (var client = new WebClient())
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                return Encoding.ASCII.GetString(client.UploadFile(apiPath, filePath));
            }
        }

        public static string DownloadFile(string remoteLocation, string localLocation)
        {
            using (var client = new WebClient())
            {
                var returnValue = new List<string>();
                remoteLocation = HttpUtility.HtmlDecode(remoteLocation);
                var fileNames = remoteLocation.Split(' ');

                foreach (var file in fileNames)
                {
                    var fileName = Path.Combine(localLocation, file.Split('/').Last());
                    Trace.TraceInformation($"remote: {file}, local: {fileName}");
                    client.DownloadFile(file, fileName);
                    var fileContent = File.ReadAllText(fileName);
                    if (fileContent.Contains("Bewerbungsrenner | Anmelden"))
                    {
                        throw new InvalidOperationException("authentication failed");
                    }

                    returnValue.Add(fileName);
                    if (File.Exists(fileName))
                    {
                        Trace.TraceInformation("done...");
                    }
                    else
                    {
                        throw new InvalidOperationException($"File {file} wasn't downloaded...");
                    }
                }
                var returnStr = string.Join(" ", returnValue);
                Trace.TraceInformation($"returning: {returnStr}");
                return returnStr;
            }
        }
    }
}
