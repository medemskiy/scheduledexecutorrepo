﻿using System;

namespace ScheduledExecutor
{
    public class ScenarioExecutionFailedException: Exception
    {
        public ScenarioExecutionFailedException() 
        {
        }

        public ScenarioExecutionFailedException(string message) : base(message)
        {
        }

        public ScenarioExecutionFailedException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
