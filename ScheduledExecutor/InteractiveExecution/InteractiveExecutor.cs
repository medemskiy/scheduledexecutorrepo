﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using OpenQA.Selenium.Firefox;
using ScheduledExecutor.Wrappers;
using ScheduledExecutor.Utils;
using Newtonsoft.Json.Linq;

namespace ScheduledExecutor
{
    public static class InteractiveExecutor
    {
        public static int ConsecuitiveInitFailures = 0;
        public static int ConsecuitiveInitFailuresReport = 5;

        public static int ConsecuitiveFailures = 0;
        public static int ConsecuitiveFailuresReport = 10;
        public static void Start()
        {
            var version = GetDriverVersion();
            var worker = new HttpListenerWorker();
            worker.DriverVersion = version;
            worker.Initialize();
            Console.WriteLine("Waiting for command...");
            while (true)
            {
                var command = worker.GetCommand().Result;

                if (command == null)
                {
                    Thread.Sleep(1000);
                    continue;
                }

                try
                {
                    using (var driver = new SeleniumWrapper(typeof(FirefoxDriver), command))
                    {
                        IDEScenario scenario = null;

                        try
                        {
                            var rawscenario = GetScenarioHtml(command.ScenarioLocation.AbsoluteUri).Trim();
                            scenario = ParseScenario(rawscenario);

                            if (scenario == null)
                            {
                                throw new InvalidDataException(
                                    $"Can't parse scenario. Scenario location: {command.ScenarioLocation.AbsoluteUri}. Scenario string: {rawscenario}");
                            }

                            scenario.UplaodTargets = command.UploadTargets;

                            Common.SaveScenario(scenario);
                            driver.ExecuteIdeScenario(scenario);

                            var status = worker.ProcessedApiRequests.FirstOrDefault(cmd => cmd.Id == command.Id).ExecutionStatus;
                            status.Status = Status.Passed;
                            status.Note = "ok";

                            ConsecuitiveFailures = 0;
                        }
                        catch (ScenarioExecutionFailedException ex)
                        {
                            var exc = ex.InnerException ?? ex;
                            Trace.TraceError("Command execution failed");
                            var status = worker.ProcessedApiRequests.FirstOrDefault(cmd => cmd.Id == command.Id).ExecutionStatus;
                            status.Status = Status.Failed;
                            status.Note = $"Scenario failed: {exc.Message}||{exc.StackTrace}";
                            ConsecuitiveFailures++;
                        }
                        catch (TaskCanceledException)
                        {
                            Trace.TraceError("Execution timed out");
                            var status = worker.ProcessedApiRequests.FirstOrDefault(cmd => cmd.Id == command.Id).ExecutionStatus;
                            status.Status = Status.Failed;
                            status.Note = "Timed out";
                            ConsecuitiveFailures++;
                        }
                        catch (Exception ex)
                        {
                            while (ex.InnerException != null)
                            {
                                ex = ex.InnerException ?? ex;
                            }
                        
                        Trace.TraceError(ex.ToString());
                            var status = worker.ProcessedApiRequests.FirstOrDefault(cmd => cmd.Id == command.Id).ExecutionStatus;
                            status.Status = Status.Failed;
                            status.Note = $"{ex.Message}||{ex.StackTrace}";
                            ConsecuitiveFailures++;
                        }
                        finally
                        {
                            if (scenario != null)
                            {
                                driver.TakeScreenShot($"PostScenario_{scenario.Name}");
                                driver.SavePageSource($"PostScenario_{scenario.Name}");
                            }
                            var status = worker.ProcessedApiRequests.FirstOrDefault(cmd => cmd.Id == command.Id).ExecutionStatus;

                            Trace.TraceInformation($"execution_result: {JsonConvert.SerializeObject(status)}");

                            Console.WriteLine($"execution_result: {JsonConvert.SerializeObject(status)}");

                            FileSystem.CreateStatusFile(JsonConvert.SerializeObject(status));
                            
                            FileSystem.ArchiveArtifacts();

                            DoCallBack(command.CallBackUri.AbsoluteUri);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Trace.TraceError("Scenario level exception: " + ex);
                    var status = worker.ProcessedApiRequests.FirstOrDefault(cmd => cmd.Id == command.Id).ExecutionStatus;
                    status.Status = Status.Failed;
                    status.Note = $"Scenario level exception: {ex.Message}||{ex.StackTrace}";

                    if (ex.Message.Contains("Cannot start the driver service on http://localhost"))
                    {
                        ConsecuitiveInitFailures++;
                    }

                    ConsecuitiveFailures++;
                }
                finally
                {
                    Thread.Sleep(2000);
                    Process.Start("cmd.exe", "/C taskkill /im firefox* /t /f");
                    Process.Start("cmd.exe", "/C taskkill /im werfault* /t /f");
                    //Process.Start("cmd.exe", "/C taskkill /im geckodriver* /t /f");

                    Console.WriteLine("---------- scenario execution finished ----------");
                    Console.WriteLine("");
                    Console.WriteLine($"Last {ConsecuitiveFailures} execution(s) failed...");

                    if (ConsecuitiveFailures > ConsecuitiveFailuresReport)
                    {
                        ReportHealthIssue();

                        if (ConsecuitiveInitFailures > ConsecuitiveInitFailuresReport)
                        {
                            Trace.TraceError("Too many init problems, trying to restart...");
                            Environment.Exit(1);
                        }
                    }

                    Console.WriteLine("Waiting for command...");
                }
            }
        }

        private static string GetScenarioHtml(string scenarioUri)
        {
            try
            {
                var scenario = RequestWrapper.Get(scenarioUri);
                return scenario;
            }
            catch
            {
                return string.Empty;
            }
        }

        private static IDEScenario ParseScenario(string rawScenario)
        {
            if (string.IsNullOrEmpty(rawScenario))
                return null;
            
            if (rawScenario.StartsWith("<?xml") || rawScenario.StartsWith("<!DOCTYPE") || rawScenario.StartsWith("<html xmlns"))
            {
                return ParseXML(rawScenario);
            }
            else if(JObject.Parse(rawScenario) != null)
            {
                return ParseJson(rawScenario);
            }
            else
            {
                return ParseScript(rawScenario);
            }

        }

        private static IDEScenario ParseXML(string rawScenario)
        {
            var scenario = rawScenario.Replace("\t", "").Replace("\n", "").Replace("\r", "");
            var baseUrlRegex = new Regex("<link rel=\"selenium.base\" href=\"(.*?)\" />");
            var titelRegex = new Regex(@"<title>(.*?)</title>");
            var stepsRegex = new Regex(@"<tbody>(.*?)</tbody>");
            var stepRegex = new Regex(@"<tr>(.*?)</tr>");
            var partRegex = new Regex(@"<td>(.*?)</td>");

            var baseUrl = baseUrlRegex.Match(scenario).Groups[1].Value;
            var title = titelRegex.Match(scenario).Groups[1].Value;
            var steps = stepsRegex.Match(scenario).Groups[1].Value;
            var stepsraw = stepRegex.Matches(steps);

            var cmds = new List<IDECommand>();

            foreach (var stepraw in stepsraw)
            {
                var step = partRegex.Matches(stepraw.ToString());
                cmds.Add(
                    new IDECommand
                    {
                        Command = HttpUtility.HtmlDecode(step[0].Groups[1].Value),
                        Target = HttpUtility.HtmlDecode(step[1].Groups[1].Value),
                        Value = HttpUtility.HtmlDecode(step[2].Groups[1].Value)
                    });
            }

            return new IDEScenario {Name = title, Commands = cmds, RawScenario = rawScenario, BaseUrl = baseUrl};
        }

        private static IDEScenario ParseJson(string rawScenario)
        {
            var sceanrio = JObject.Parse(rawScenario);
            
            var baseUrl = sceanrio["url"].ToString();
            var title = sceanrio["name"].ToString();
            var steps = sceanrio["tests"].FirstOrDefault()["commands"];

            var cmds = new List<IDECommand>();

            foreach (var step in steps)
            {
                cmds.Add(
                    new IDECommand
                    {
                        Command = step["command"].ToString(),// HttpUtility.HtmlDecode(step[0].Groups[1].Value),
                        Target = step["target"].ToString(),//HttpUtility.HtmlDecode(step[1].Groups[1].Value),
                        Value = step["value"].ToString()//HttpUtility.HtmlDecode(step[2].Groups[1].Value)
                    });
            }

            return new IDEScenario { Name = title, Commands = cmds, RawScenario = rawScenario, BaseUrl = baseUrl };
        }

        private static IDEScenario ParseScript(string rawScenario)
        {
            return new IDEScenario {};
        }

        public static string GetDriverVersion()
        {
            try
            {
                return
                    File.ReadAllText(
                        Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
                            "revision"));
            }
            catch (Exception)
            {
                return "unknown";
            }
        }

        public static void ReportHealthIssue()
        {
            try
            {
                RequestWrapper.Get("https://2017b.bewerbungsrenner.de/ScriptedBrowserUtils/workerRunnerHasIssues");
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
            finally
            {
                ConsecuitiveFailures = 0;
            }
        }

        public static void DoCallBack(string callBackUri)
        {
            Trace.TraceInformation(callBackUri);
            var zipedResults = Path.Combine(FileSystem.ScenarioTempFolder, "artifacts.zip");

            if (!File.Exists(zipedResults))
            {
                throw new FileNotFoundException($"Can't find artifacts archive by path \"{zipedResults}\"");
            }
            try
            {
                var response = RequestWrapper.Post(callBackUri, zipedResults);
                Trace.TraceInformation(response);
                Console.WriteLine($"Callback response: {response}");
            }
            catch (Exception ex)
            {
                Trace.TraceWarning($"Callback attempt failed ({ex.Message}), retrying...");
                Thread.Sleep(15000);
                try
                {
                    var response = RequestWrapper.Post(callBackUri, zipedResults);
                    Trace.TraceInformation(response);
                    Console.WriteLine($"Callback response: {response}");
                }
                catch (Exception internalex)
                {
                    Trace.TraceError(internalex.ToString());
                    File.Copy(zipedResults, Path.Combine(FileSystem.FolderForFailedTasks, $"{DateTime.Now:yyyyMMddHHmmss}-report.zip"));
                }
            }
        }
    }
}
