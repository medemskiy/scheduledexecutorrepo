﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using UIAutomationClient;
using System.Windows;

namespace ScheduledExecutor.UIAutomation
{
    public class AutomationElement
    {
        #region IUIAutomation native
        private static IUIAutomation2 uiAutomation = new CUIAutomation8();
        private IUIAutomationElement uiaElement;
        private List<AutomationPattern> supportedPatterns;

        [DllImport("User32.dll")]
        static extern IntPtr GetDC(IntPtr hwnd);

        [DllImport("User32.dll")]
        static extern int ReleaseDC(IntPtr hwnd, IntPtr dc);
        #endregion IUIAutomation native

        #region Tree scope properties
        public static AutomationElement RootElement = new AutomationElement(uiAutomation.GetRootElement());
        public AutomationElement Parent => new AutomationElement(uiAutomation.ControlViewWalker.GetParentElement(uiaElement));
        public IEnumerable<AutomationElement> Children => IUIAutomationElementArrayToIEnumerable(uiaElement.FindAll(TreeScope.TreeScope_Children, uiAutomation.CreateTrueCondition()));
        public IEnumerable<AutomationElement> Descendants => IUIAutomationElementArrayToIEnumerable(uiaElement.FindAll(TreeScope.TreeScope_Descendants, uiAutomation.CreateTrueCondition()));
        #endregion Tree scope properties

        #region Element properties
        public string AcceleratorKey => uiaElement?.CurrentAcceleratorKey;
        public string AccessKey => uiaElement?.CurrentAccessKey;
        public string AriaProperties => uiaElement?.CurrentAriaProperties;
        public string AriaRole => uiaElement?.CurrentAriaRole;
        public string AutomationId => uiaElement?.CurrentAutomationId;
        public Rect BoundingRectangle => tagRECTToRect(uiaElement.CurrentBoundingRectangle);
        public string ClassName => uiaElement?.CurrentClassName;
        public IEnumerable<AutomationElement> ControllerFor => IUIAutomationElementArrayToIEnumerable(uiaElement.CurrentControllerFor);
        public ControlType ControlType => uiaElement == null ? ControlType.Null : (ControlType)uiaElement.CurrentControlType;
        public int Culture => uiaElement?.CurrentCulture ?? -1;
        public IEnumerable<AutomationElement> DescribedBy => IUIAutomationElementArrayToIEnumerable(uiaElement.CurrentDescribedBy);
        public IEnumerable<AutomationElement> FlowsTo => IUIAutomationElementArrayToIEnumerable(uiaElement.CurrentFlowsTo);
        public string FrameworkId => uiaElement?.CurrentFrameworkId;
        public bool HasKeyboardFocus => uiaElement?.CurrentHasKeyboardFocus == 1;
        public string HelpText => uiaElement?.CurrentHelpText;
        public bool IsContentElement => uiaElement?.CurrentIsContentElement == 1;
        public bool IsControlElement => uiaElement?.CurrentIsControlElement == 1;
        public bool IsDataValidForForm => uiaElement?.CurrentIsDataValidForForm == 1;
        public bool IsEnabled => uiaElement?.CurrentIsEnabled == 1;
        public bool IsKeyboardFocusable => uiaElement?.CurrentIsKeyboardFocusable == 1;
        public bool IsOffscreen => uiaElement?.CurrentIsOffscreen == 1;
        public bool IsPassword => uiaElement?.CurrentIsPassword == 1;
        public bool IsRequiredForForm => uiaElement?.CurrentIsRequiredForForm == 1;
        public string ItemStatus => uiaElement?.CurrentItemStatus;
        public string ItemType => uiaElement?.CurrentItemStatus;
        public AutomationElement LabeledBy => new AutomationElement(uiaElement.CurrentLabeledBy);
        public string LocalizedControlType => uiaElement?.CurrentLocalizedControlType;
        public string Name => uiaElement?.CurrentName;
        public string NativeWindowHandle => uiaElement?.CurrentNativeWindowHandle.ToString();
        public OrientationType Orientation => uiaElement.CurrentOrientation;
        public int ProcessId => uiaElement?.CurrentProcessId ?? -1;
        public string ProviderDescription => uiaElement?.CurrentProviderDescription;
        public System.Windows.Point GetClickablePoint()
        {
            if (uiaElement == null)
            {
                return new System.Windows.Point(-1, -1);
            }

            tagPOINT clickablePoint;
            uiaElement.GetClickablePoint(out clickablePoint);

            return new System.Windows.Point(clickablePoint.x, clickablePoint.y);
        }
        #endregion Element properties   

        #region Element patterns
        public bool IsAnnotationPatternAvailable => supportedPatterns.Contains(AutomationPattern.Annotation);
        public bool IsCustomNavigationPatternAvailable => supportedPatterns.Contains(AutomationPattern.CustomNavigation);
        public bool IsDragPatternAvailable => supportedPatterns.Contains(AutomationPattern.Drag);
        public bool IsDockPatternAvailable => supportedPatterns.Contains(AutomationPattern.Dock);
        public bool IsDropTargetPatternAvailable => supportedPatterns.Contains(AutomationPattern.DropTarget);
        public bool IsExpandCollapsePatternAvailable => supportedPatterns.Contains(AutomationPattern.ExpandCollapse);
        public bool IsGridItemPatternAvailable => supportedPatterns.Contains(AutomationPattern.GridItem);
        public bool IsGridPatternAvailable => supportedPatterns.Contains(AutomationPattern.Grid);
        public bool IsInvokePatternAvailable => supportedPatterns.Contains(AutomationPattern.Invoke);
        public bool IsItemContainerPatternAvailable => supportedPatterns.Contains(AutomationPattern.ItemContainer);
        public bool IsLegacyIAccessiblePatternAvailable => supportedPatterns.Contains(AutomationPattern.LegacyIAccessible);
        public bool IsMultipleViewPatternAvailable => supportedPatterns.Contains(AutomationPattern.MultipleView);
        public bool IsObjectModelPatternAvailable => supportedPatterns.Contains(AutomationPattern.ObjectModel);
        public bool IsRangeValuePatternAvailable => supportedPatterns.Contains(AutomationPattern.RangeValue);
        public bool IsScrollItemPatternAvailable => supportedPatterns.Contains(AutomationPattern.ScrollItem);
        public bool IsScrollPatternAvailable => supportedPatterns.Contains(AutomationPattern.Scroll);
        public bool IsSelectionItemPatternAvailable => supportedPatterns.Contains(AutomationPattern.SelectionItem);
        public bool IsSelectionPatternAvailable => supportedPatterns.Contains(AutomationPattern.Selection);
        public bool IsSpreadsheetItemPatternAvailable => supportedPatterns.Contains(AutomationPattern.SpreadsheetItem);
        public bool IsSpreadsheetPatternAvailable => supportedPatterns.Contains(AutomationPattern.Spreadsheet);
        public bool IsStylesPatternAvailable => supportedPatterns.Contains(AutomationPattern.Styles);
        public bool IsSynchronizedInputPatternAvailable => supportedPatterns.Contains(AutomationPattern.SynchronizedInput);
        public bool IsTableItemPatternAvailable => supportedPatterns.Contains(AutomationPattern.TableItem);
        public bool IsTablePatternAvailable => supportedPatterns.Contains(AutomationPattern.Table);
        public bool IsTextChildPatternAvailable => supportedPatterns.Contains(AutomationPattern.TextChild);
        public bool IsTextEditPatternAvailable => supportedPatterns.Contains(AutomationPattern.TextEdit);
        public bool IsTextPatternAvailable => supportedPatterns.Contains(AutomationPattern.Text);
        public bool IsTextPattern2Available => supportedPatterns.Contains(AutomationPattern.Text2);
        public bool IsTogglePatternAvailable => supportedPatterns.Contains(AutomationPattern.Toggle);
        public bool IsTransformPatternAvailable => supportedPatterns.Contains(AutomationPattern.Transform);
        public bool IsTransform2PatternAvailable => supportedPatterns.Contains(AutomationPattern.Transform2);
        public bool IsValuePatternAvailable => supportedPatterns.Contains(AutomationPattern.Value);
        public bool IsVirtualizedItemPatternAvailable => supportedPatterns.Contains(AutomationPattern.VirtualizedItem);
        public bool IsWindowPatternAvailable => supportedPatterns.Contains(AutomationPattern.Window);
        #endregion Element patterns

        public AutomationElement(IUIAutomationElement element)
        {
            uiaElement = element;
            supportedPatterns = GetSupportedPatterns();
        }

        #region Generic getters
        public IEnumerable<AutomationElement> FindElementsByCondition(TreeScope treeScope, IUIAutomationCondition condition)
        {
            return IUIAutomationElementArrayToIEnumerable(uiaElement.FindAll(treeScope, condition));
        }

        public IEnumerable<AutomationElement> FindElementsByCondition(TreeScope treeScope, AutomationCondition condition)
        {
            return FindElementsByCondition(treeScope, condition.Condition);
        }
        public AutomationElement FindElementByCondition(TreeScope treeScope, IUIAutomationCondition condition)
        {
            return new AutomationElement(uiaElement.FindFirst(treeScope, condition));
        }

        public AutomationElement FindElementByCondition(TreeScope treeScope, AutomationCondition condition)
        {
            return FindElementByCondition(treeScope, condition.Condition);
        }

        public AutomationElement FindElementByPropertyValue(TreeScope treeScope, AutomationProperty property, string value)
        {
            return FindElementByCondition(treeScope, uiAutomation.CreatePropertyCondition(AutomationCondition.GetPropertyByName(property.ToString()), value));
        }

        public IEnumerable<AutomationElement> FindElementsByPropertyValue(TreeScope treeScope, AutomationProperty property, string value)
        {
            return FindElementsByCondition(treeScope, uiAutomation.CreatePropertyCondition(AutomationCondition.GetPropertyByName(property.ToString()), value));
        }

        public AutomationElement GetElementByUiMapPath(IEnumerable<UiElement> mapPath)
        {
            var currentElement = new AutomationElement(uiaElement);

            //mapPath.Remove(mapPath.FirstOrDefault(el => el.Name.Equals("SkypeRootStub")));

            foreach (var el in mapPath)
            {
                Trace.TraceInformation($"Processing {el.Name}");

                if (el.Name.Equals("SkypeRootStub")) continue;

                var scopeStr = $"TreeScope_{el.Data["TreeScope"]}";

                TreeScope treeScope;

                Enum.TryParse(scopeStr, out treeScope);

                var condition = new AutomationCondition(el);

                try
                {
                    currentElement = currentElement?.FindElementByCondition(treeScope, condition);
                }
                catch (Exception ex)
                {
                    Trace.TraceError($"Element {string.Join(", ", el.Data.Select(prop => $"{prop.Key}={prop.Value}"))} not found. Error: {ex.Message}");
                }
            }

            return currentElement;
        }
        #endregion Generic getters

        #region Getters (property value)
        public IEnumerable<AutomationElement> FindElementsByClassName(TreeScope treeScope, string className)
        {
            return FindElementsByPropertyValue(treeScope, AutomationProperty.ClassName, className);
        }

        public AutomationElement FindElementByClassName(TreeScope treeScope, string className)
        {
            return FindElementByPropertyValue(treeScope, AutomationProperty.ClassName, className);
        }

        public IEnumerable<AutomationElement> FindElementsByAutomationId(TreeScope treeScope, string automationId)
        {
            return FindElementsByPropertyValue(treeScope, AutomationProperty.AutomationId, automationId);
        }

        public AutomationElement FindElementByAutomationId(TreeScope treeScope, string automationId)
        {
            return FindElementByPropertyValue(treeScope, AutomationProperty.AutomationId, automationId);
        }

        public IEnumerable<AutomationElement> FindElementsByName(TreeScope treeScope, string name)
        {
            return FindElementsByPropertyValue(treeScope, AutomationProperty.Name, name);
        }

        public AutomationElement FindElementByName(TreeScope treeScope, string name)
        {
            return FindElementByPropertyValue(treeScope, AutomationProperty.Name, name);
        }
        #endregion Getters (property value)

        #region Pattern checker
        public List<AutomationPattern> GetSupportedPatterns()
        {
            if (uiaElement == null)
            {
                return new List<AutomationPattern>();
            }

            int[] ids;
            string[] names;

            uiAutomation.PollForPotentialSupportedPatterns(uiaElement, out ids, out names);

            return ids.OfType<int>().ToList().Select(pid => (AutomationPattern)pid).ToList();
        }
        #endregion Pattern checker

        #region Pattern actions
        private void InvokePatternInvoke()
        {
            if (!IsInvokePatternAvailable)
            {
                throw new InvalidOperationException("Invoke pattern is not supported");
            }

            var ptrn = (IUIAutomationInvokePattern)uiaElement.GetCurrentPattern((int)AutomationPattern.Invoke);
            ptrn.Invoke();
        }

        private void TogglePatternToggle()
        {
            if (!IsTogglePatternAvailable)
            {
                throw new InvalidOperationException("Toggle pattern is not supported");
            }

            var ptrn = (IUIAutomationTogglePattern)uiaElement.GetCurrentPattern((int)AutomationPattern.Toggle);
            ptrn.Toggle();
        }

        private ToggleState TogglePatternGetState()
        {
            if (!IsTogglePatternAvailable)
            {
                throw new InvalidOperationException("Toggle pattern is not supported");
            }

            var ptrn = (IUIAutomationTogglePattern)uiaElement.GetCurrentPattern((int)AutomationPattern.Toggle);
            return ptrn.CurrentToggleState;
        }

        private void ExpandCollapsePatternExpand()
        {
            if (!IsExpandCollapsePatternAvailable)
            {
                throw new InvalidOperationException("ExpandCollapse pattern is not supported");
            }

            var ptrn = (IUIAutomationExpandCollapsePattern)uiaElement.GetCurrentPattern((int)AutomationPattern.ExpandCollapse);
            ptrn.Expand();
        }

        private void ExpandCollapsePatternCollapse()
        {
            if (!IsExpandCollapsePatternAvailable)
            {
                throw new InvalidOperationException("ExpandCollapse pattern is not supported");
            }

            var ptrn = (IUIAutomationExpandCollapsePattern)uiaElement.GetCurrentPattern((int)AutomationPattern.ExpandCollapse);
            ptrn.Collapse();
        }

        private ExpandCollapseState ExpandCollapsePatternGetState()
        {
            if (!IsExpandCollapsePatternAvailable)
            {
                throw new InvalidOperationException("ExpandCollapse pattern is not supported");
            }

            var ptrn = (IUIAutomationExpandCollapsePattern)uiaElement.GetCurrentPattern((int)AutomationPattern.ExpandCollapse);
            return ptrn.CurrentExpandCollapseState;
        }

        private string ValuePatternGetValue()
        {
            if (!IsValuePatternAvailable)
            {
                throw new InvalidOperationException("Value pattern is not supported");
            }

            var ptrn = (IUIAutomationValuePattern)uiaElement.GetCurrentPattern((int)AutomationPattern.Value);
            return ptrn.CurrentValue;
        }

        private void ValuePatternSetValue(string value)
        {
            if (!IsValuePatternAvailable)
            {
                throw new InvalidOperationException("Value pattern is not supported");
            }

            var ptrn = (IUIAutomationValuePattern)uiaElement.GetCurrentPattern((int)AutomationPattern.Value);
            ptrn.SetValue(value);
        }

        private bool ValuePatternIsReadonly()
        {
            if (!IsValuePatternAvailable)
            {
                throw new InvalidOperationException("Value pattern is not supported");
            }

            var ptrn = (IUIAutomationValuePattern)uiaElement.GetCurrentPattern((int)AutomationPattern.Value);
            return ptrn.CurrentIsReadOnly == 1;
        }

        private void WindowPatternSetVisualState(WindowVisualState state)
        {
            if (!IsWindowPatternAvailable)
            {
                throw new InvalidOperationException("Window pattern is not supported");
            }

            var ptrn = (IUIAutomationWindowPattern)uiaElement.GetCurrentPattern((int)AutomationPattern.Window);
            ptrn.SetWindowVisualState(state);
        }

        private void WindowPatternClose()
        {
            if (!IsWindowPatternAvailable)
            {
                throw new InvalidOperationException("Window pattern is not supported");
            }

            var ptrn = (IUIAutomationWindowPattern)uiaElement.GetCurrentPattern((int)AutomationPattern.Window);
            Thread.Sleep(1000);

            ptrn.Close();
        }

        #endregion Pattern actions

        #region Actions
        public void InvokeClick()
        {
            InvokePatternInvoke();
        }

        public void Click()
        {
            MouseHover();
            MouseWrapper.Click();
        }

        public void Click(int x, int y)
        {
            MouseHover(x, y);
            MouseWrapper.Click();
        }

        public void RightClick()
        {
            MouseHover();
            MouseWrapper.RightClick();
        }

        public void RightClick(int x, int y)
        {
            MouseHover(x, y);
            MouseWrapper.RightClick();
        }

        public void MouseHover(int dx = 0, int dy = 0)
        {
            var cords = dx != 0 || dy != 0
                ? new System.Windows.Point(uiaElement.CurrentBoundingRectangle.left, uiaElement.CurrentBoundingRectangle.top)
                : GetClickablePoint();

            var x = (int)cords.X;
            var y = (int)cords.Y;

            MouseWrapper.MoveTo(x + dx, y + dy);
        }

        public void DragDropTo(AutomationElement target, int delay = 500)
        {
            MouseHover();
            Thread.Sleep(delay);

            MouseWrapper.LeftButtonPress();

            Thread.Sleep(delay);

            target.MouseHover();
            Thread.Sleep(delay);

            MouseWrapper.LeftButtonRelease();
        }

        public void Focus()
        {
            uiaElement.SetFocus();
        }

        public void SendKeys(string text, int delay = 0)
        {
            uiaElement.SetFocus();

            var specialChars = new List<string> { "+", "^", "%", "~", "(", ")", "{", "}" };

            foreach (var ch in text.ToCharArray())
            {
                var str = ch.ToString();
                System.Windows.Forms.SendKeys.SendWait(specialChars.Contains(str) ? $"{{{str}}}" : str);
                Thread.Sleep(delay);
            }
        }

        public string GetValue()
        {
            return IsValuePatternAvailable ? ValuePatternGetValue() : string.Empty;
        }

        public void SetValue(string value)
        {
            ValuePatternSetValue(value);
        }

        public void ClearValue()
        {
            ValuePatternSetValue("");
        }

        public void Highlight() // TODO: update with clearing 
        {
            var desktopPtr = GetDC(IntPtr.Zero);

            var rect = new Rectangle((int)BoundingRectangle.X, (int)BoundingRectangle.Y, (int)BoundingRectangle.Width, (int)BoundingRectangle.Height);
            using (var g = Graphics.FromHwnd(IntPtr.Zero))
            {
                var pen = new Pen(Color.RoyalBlue, 6);

                g.DrawRectangle(pen, rect);
            }

            ReleaseDC(IntPtr.Zero, desktopPtr);
        }

        public void Maximize()
        {
            WindowPatternSetVisualState(WindowVisualState.WindowVisualState_Maximized);
        }

        public void Minimize()
        {
            WindowPatternSetVisualState(WindowVisualState.WindowVisualState_Minimized);
        }

        public void Restore()
        {
            WindowPatternSetVisualState(WindowVisualState.WindowVisualState_Normal);
        }

        public void Close()
        {
            WindowPatternClose();
        }

        public bool Exists()
        {
            return ClassName == null ? false : true;
        }

        #endregion Actions

        #region Converters
        private IEnumerable<AutomationElement> IUIAutomationElementArrayToIEnumerable(IUIAutomationElementArray array)
        {
            var output = new List<AutomationElement>();

            if (array == null)
            {
                return output;
            }

            for (var i = 0; i < array.Length; i++)
            {
                output.Add(new AutomationElement(array.GetElement(i)));
            }

            return output;
        }

        private Rect tagRECTToRect(tagRECT rect)
        {
            return new Rect(x: rect.left, y: rect.top, width: rect.right - rect.left, height: rect.bottom - rect.top);
        }
        #endregion Converters
    }
}
