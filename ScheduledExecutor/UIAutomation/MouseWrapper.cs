﻿using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ScheduledExecutor.UIAutomation
{
    public static class MouseWrapper
    {
        [DllImport("user32.dll")]
        static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

        private const int MoveEvent = 0x0001;
        private const int LeftButtonDown = 0x0002;
        private const int LeftButtonUp = 0x0004;
        private const int RightButtonDown = 0x0008;
        private const int RightButtonUp = 0x0010;
        private const int AbsoluteEvent = 0x8000;

        public static void Move(int xDelta, int yDelta)
        {
            mouse_event(MoveEvent, xDelta, yDelta, 0, 0);
        }

        public static void MoveTo(int x, int y)
        {
            float min = 0;
            float max = 65535;
            var resolution = Screen.PrimaryScreen.Bounds;
            x = (int)Remap(x, 0.0f, resolution.Width, min, max);
            y = (int)Remap(y, 0.0f, resolution.Height, min, max);

            mouse_event(AbsoluteEvent | MoveEvent, x, y, 0, 0);
        }

        private static void MouseDown(int mouseButton)
        {
            mouse_event(mouseButton, 0, 0, 0, 0);
        }

        private static void MouseUp(int mouseButton)
        {
            mouse_event(mouseButton, 0, 0, 0, 0);
        }

        public static void LeftButtonPress()
        {
            MouseDown(LeftButtonDown);
        }

        public static void LeftButtonRelease()
        {
            MouseUp(LeftButtonUp);
        }

        public static void RightButtonPress()
        {
            MouseDown(LeftButtonDown);
        }

        public static void RightButtonRelease()
        {
            MouseUp(RightButtonUp);
        }

        public static void Click()
        {
            MouseDown(LeftButtonDown);
            MouseUp(LeftButtonUp);
        }

        public static void RightClick()
        {
            MouseDown(RightButtonDown);
            MouseUp(RightButtonUp);
        }

        private static float Remap(float value, float from1, float to1, float from2, float to2)
        {
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        }
    }
}
