﻿using System.Collections.Generic;

namespace ScheduledExecutor.UIAutomation
{
    /// <summary>
    /// Simple object to represent a single UI element node in a UI map.
    /// </summary>
    public class UiElement
    {
        public string Name;
        public string Type;
        public Dictionary<string, object> Data;
        public List<UiElement> ChildElements = new List<UiElement>();

        /// <summary>
        /// Returns a shallow copy of this object (a copy without any child elements)
        /// </summary>
        public UiElement ShallowCopy()
        {
            var newElement = new UiElement()
            {
                Name = Name,
                Type = Type
            };

            if (Data != null)
            {
                newElement.Data = new Dictionary<string, object>();
                foreach (var d in Data)
                {
                    newElement.Data.Add(d.Key, d.Value);
                }
            }
            else
            {
                newElement.Data = null;
            }
            newElement.ChildElements = null;
            return newElement;
        }
    }
}