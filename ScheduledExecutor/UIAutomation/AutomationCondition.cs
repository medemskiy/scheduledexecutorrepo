﻿using System;
using UIAutomationClient;

namespace ScheduledExecutor.UIAutomation
{
    public class AutomationCondition
    {
        private static IUIAutomation2 uiAutomation = new CUIAutomation8();
        public IUIAutomationCondition Condition { get; set; }

        public AutomationCondition()
        {
        }

        public AutomationCondition(UiElement elementMap)
        {
            foreach (var property in elementMap.Data)
            {
                if (property.Key.ToLower().Equals("treescope"))
                {
                    continue;
                }

                Condition =
                    Condition == null
                    ? CreateCondition(property.Key, (string)property.Value).Condition
                    : And(property.Key, (string)property.Value).Condition;
            }
        }

        public AutomationCondition(string propertyName, string propertyValue)
        {
            Condition = Condition == null
                ? CreateCondition(propertyName, propertyValue).Condition
                : And(propertyName, propertyValue).Condition;
        }

        public AutomationCondition CreateCondition(string propertyName, string propertyValue)
        {
            Condition = new CUIAutomation().CreatePropertyCondition(GetPropertyByName(propertyName), propertyValue);
            return this;
        }

        public AutomationCondition And(string propertyName, string propertyValue)
        {
            Condition = new CUIAutomation().CreateAndCondition(Condition, uiAutomation.CreatePropertyCondition(GetPropertyByName(propertyName), propertyValue));
            return this;
        }

        public AutomationCondition Or(string propertyName, string propertyValue)
        {
            Condition = new CUIAutomation().CreateOrCondition(Condition, uiAutomation.CreatePropertyCondition(GetPropertyByName(propertyName), propertyValue));
            return this;
        }

        public static int GetPropertyByName(string propertyName)
        {
            AutomationProperty prop;
            Enum.TryParse(propertyName, true, out prop);

            return (int)prop;
        }
    }
}
