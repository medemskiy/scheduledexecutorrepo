﻿using Newtonsoft.Json.Linq;
using ScheduledExecutor.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ScheduledExecutor.GitHub
{
    public static class GitHubClient
    {
        public static void DownloadGeckoDriver(string release = "latest")
        {
            try
            {
                var gitHubApiEndpoint = "https://api.github.com";

                var owner = "mozilla";
                var repo = "geckodriver";

                using (var client = new WebClient())
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                    client.Headers.Set("Accept", "application/vnd.github.v3+json");
                    var response = client.DownloadString($"{gitHubApiEndpoint}/repos/{owner}/{repo}/releases/{(release.Equals("latest") ? "latest" : $"tags/{release}")}");
                    var jresp = JObject.Parse(response);

                    var assetsList = jresp["assets"];

                    var x64winAsset = assetsList.Select(a => a["browser_download_url"].ToString()).Where(a => a.Contains("win32.zip")).FirstOrDefault();

                    var geckoZip = Path.Combine(FileSystem.UserFolder, "gecko.zip");

                    client.DownloadFile(x64winAsset, geckoZip);

                    var geckoExe = Path.Combine(FileSystem.WorkingFolder, "geckodriver.exe");
                    if (File.Exists(geckoExe))
                    {
                        File.Delete(geckoExe);
                    }

                    ZipFile.ExtractToDirectory(geckoZip, FileSystem.WorkingFolder);
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Cannot download latest gecko: {ex}");
            }
        }
    }
}
