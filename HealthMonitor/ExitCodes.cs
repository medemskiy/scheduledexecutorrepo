﻿namespace HealthMonitor
{
    public enum ExitCodes
    {
        Update = 777,
        RunToEnd = 0,
        UserRequested = -1073741510,
        PlannedKill = 333
    }
}
