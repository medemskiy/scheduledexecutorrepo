﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace HealthMonitor
{
    class Program
    {
        public static string ownLocation =
            Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        public static string revisionsRoot = Path.Combine(ownLocation, "revisions");
        public static string latest = Path.Combine(revisionsRoot, "latest");
        public static string lastStable = Path.Combine(revisionsRoot, "laststable");
        public static string monitoringTarget = "ScheduledExecutor.exe";
        public static bool updateInitiated;

        static void Main(string[] args)
        {
            var swLifetime = Stopwatch.StartNew();
            var swUpdate = new Stopwatch();

            Trace.Listeners.Clear();
            Trace.Listeners.Add(new TextWriterTraceListener($"HealthMonitor_{DateTime.Now:yyyyMMdd-HHmmss}.log"));
            Trace.AutoFlush = true;

            Console.WriteLine($"Sarted at: {DateTime.Now:F}");

            Console.WriteLine("Cleaning up...");
            Trace.TraceInformation("Cleaning up...");
            Process.Start("cmd.exe", $"/C taskkill /im {monitoringTarget.Split('.')[0]}* /t /f");
            Process.Start("cmd.exe", "/C taskkill /im firefox* /t /f");
            Process.Start("cmd.exe", "/C taskkill /im werfault* /t /f");
            Process.Start("cmd.exe", "/C taskkill /im geckodriver* /t /f");
            Thread.Sleep(5000);

            while (true)
            {
                try
                {
                    var prevStatus = 0;
                    var errorStream = string.Empty;
                    var execProcess = Process.Start(monitoringTarget);
                    execProcess.ErrorDataReceived +=
                        (sender, eventArgs) => { errorStream += eventArgs.Data + Environment.NewLine; };

                    execProcess.Exited += (sender, eventArgs) =>
                    {
                        Console.WriteLine("Cleaning up...");
                        Trace.TraceInformation("Cleaning up...");
                        //Process.Start("cmd.exe", $"/C taskkill /im {monitoringTarget.Split('.')[0]}* /t /f");
                        Process.Start("cmd.exe", "/C taskkill /im firefox* /t /f");                        
                        Process.Start("cmd.exe", "/C taskkill /im werfault* /t /f");
                        Process.Start("cmd.exe", "/C taskkill /im geckodriver* /t /f");

                        Console.WriteLine($"process exited with {execProcess.ExitCode} exit code");
                        Trace.TraceInformation($"process exited with {execProcess.ExitCode} exit code");
                        Thread.Sleep(10000);
                        switch (execProcess.ExitCode)
                        {
                            case (int)ExitCodes.Update:
                            {
                                Console.WriteLine("Starting update...");
                                Trace.TraceInformation("Starting update...");
                                swUpdate.Start();
                                var revision = Update();
                                Console.WriteLine($"Updated to {revision}");
                                Trace.TraceInformation($"Updated to {revision}");
                                break;
                            }
                            case (int)ExitCodes.PlannedKill:
                            {
                                var message = "Planned interruption. Restarting...";

                                Console.WriteLine(message);
                                Trace.TraceWarning(message);
                                break;
                            }
                            case (int)ExitCodes.RunToEnd:
                            {
                                var message = "Process exited with no errors. Restarting...";

                                Console.WriteLine(message);
                                Trace.TraceWarning(message);
                                break;
                            }
                            case (int)ExitCodes.UserRequested:
                                {
                                    var message = "Window was closed. Restarting...";

                                    Console.WriteLine(message);
                                    Trace.TraceWarning(message);
                                    break;
                                }
                            default:
                            {
                                if (updateInitiated)
                                {
                                    Console.WriteLine("Rolling back to previous stable...");
                                    Trace.TraceInformation("Rolling back to previous stable...");
                                    updateInitiated = false;
                                    var revision = Rollback();
                                    Console.WriteLine($"Rolled back to {revision}");
                                    Trace.TraceInformation($"Rolled back to {revision}");
                                }
                                else
                                {
                                    var message = $"{monitoringTarget} exited with {execProcess.ExitCode} exit code. Error stream: {errorStream}. Restarting...";

                                    Console.WriteLine(message);
                                    Trace.TraceWarning(message);
                                }
                                break;
                            }
                        }
                    };

                    while (!execProcess.HasExited)
                    {
                        if (updateInitiated &&
                            swUpdate.ElapsedMilliseconds > (int) TimeSpan.FromMinutes(30).TotalMilliseconds)
                        {
                            Console.WriteLine("Running more than 30 minutes, considering stable...");
                            Trace.TraceInformation("Running more than 30 minutes, considering stable...");
                            StoreStable();
                            updateInitiated = false;
                            swUpdate.Stop();
                        }

                        if (swLifetime.ElapsedMilliseconds - prevStatus > 60000)
                        {
                            var hb = $"Harbeat {DateTime.Now:F}";
                            Console.WriteLine(hb);
                            Trace.TraceInformation(hb);

                            prevStatus = (int) swLifetime.ElapsedMilliseconds;
                        }

                        Thread.Sleep(5000);
                    }

                }
                catch (Exception ex)
                {
                    if (updateInitiated)
                    {
                        Console.WriteLine(ex);
                        Thread.Sleep(10000);
                        swUpdate.Stop();
                        updateInitiated = false;
                        Rollback();
                    }
                    Trace.TraceInformation(ex.ToString());
                    Thread.Sleep(10000);
                }
            }
        }

        private static string Update()
        {
            updateInitiated = true;

            foreach (var file in Directory.GetFiles(latest))
            {
                try
                {
                    Trace.TraceInformation($"copying to ownLocation: {file}");
                    if (file.Contains("HealthMonitor"))
                    {
                        File.Copy(file, Path.Combine(ownLocation, Path.GetFileName(file) + "_new"), true);
                        continue;
                    }

                    File.Copy(file, Path.Combine(ownLocation, Path.GetFileName(file)), true);
                }
                catch(Exception ex)
                {
                    Console.WriteLine($"Update failed: {ex.Message}");
                    Trace.TraceError($"Update failed: {ex}");
                    return string.Empty;
                }
            }
            try
            {
                return File.ReadAllText(Path.Combine(ownLocation, "revision"));
            }
            catch (Exception)
            {
                return "unknown";
            }
            
        }

        private static string Rollback()
        {
            foreach (var file in Directory.GetFiles(latest))
            {
                Trace.TraceInformation($"deleting lastStable: {file}");
                File.Delete(file);
            }

            foreach (var file in Directory.GetFiles(lastStable))
            {
                Trace.TraceInformation($"copying to ownLocation: {file}");
                if (file.Contains("HealthMonitor"))
                {
                    Trace.TraceInformation("... skipped");
                    continue;
                }

                File.Copy(file, Path.Combine(ownLocation, Path.GetFileName(file)), true);
            }

            try
            {
                return File.ReadAllText(Path.Combine(ownLocation, "revision"));
            }
            catch (Exception)
            {
                return "unknown";
            }
        }

        private static void StoreStable()
        {
            foreach (var file in Directory.GetFiles(lastStable))
            {
                Trace.TraceInformation($"deleting lastStable: {file}");
                File.Delete(file);
            }

            foreach (var file in Directory.GetFiles(ownLocation))
            {
                Trace.TraceInformation($"copying from ownLocation: {file}");
                if (file.StartsWith("HealthMonitor") || file.EndsWith("txt") || file.EndsWith("cer"))
                {
                    Trace.TraceInformation("... skipped");
                    continue;
                }
                File.Copy(file, Path.Combine(lastStable, Path.GetFileName(file)), true);
            }
        }
    }
}
